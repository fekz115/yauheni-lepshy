package task;

import task.algorithms.Factorial;
import task.algorithms.FibonacciSequence;

import java.util.Scanner;

public class Main {

    private static Scanner consoleInput = new Scanner(System.in);

    public static final int MAX_INT_FOR_FACTORIAL = 14;
    public static final int MAX_INT_FOR_FIBONACCI = 45;

    private static final int LOOPS_COUNT = 3;
    private static final int ALGORITHMS_COUNT = 2;

    public static void main(String[] args) {
        int algorithmId, loopType, number;
        try{
            algorithmId = Integer.parseInt(args[0]);
            loopType = Integer.parseInt(args[1]);
            number = Integer.parseInt(args[2]);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e){
            algorithmId = readInt(1, ALGORITHMS_COUNT);
            loopType = readInt(1, LOOPS_COUNT);
            number = readInt(1, algorithmId == 1 ? MAX_INT_FOR_FIBONACCI : MAX_INT_FOR_FACTORIAL);
        }
        int loopTypeIndex = loopType - 1;
        switch (algorithmId){
            case 1:{
                FibonacciSequence.values()[loopTypeIndex].calculate(number);
            } break;
            case 2:{
                System.out.println(Factorial.values()[loopTypeIndex].calculate(number));
            }break;
        }
    }

    private static int readInt(){
        Scanner tempLine = new Scanner(consoleInput.nextLine());
        while(!tempLine.hasNextInt()) {
            System.out.println("Invalid input");
            tempLine = new Scanner(consoleInput.nextLine());
        }
        return tempLine.nextInt();
    }

    private static int readInt(int left, int right){
        System.out.println("Enter number between " + left + " and " + right + ":");
        int answer = readInt();
        while(answer < left || answer > right){
            System.out.println("Wrong number, it must be between " + left + " and " + right);
            answer = readInt();
        }
        return answer;
    }

}
