package task.algorithms;

public enum Factorial {

    FOR {
        @Override
        public int calculate(int number) {
            int answer = 1;
            for(int i = 1; i <= number; i++){
                answer *= i;
            }
            return answer;
        }
    }, WHILE{
        @Override
        public int calculate(int number) {
            int answer = 1;
            while(number > 0){
                answer *= number;
                number--;
            }
            return answer;
        }
    }, DOWHILE{
        @Override
        public int calculate(int number) {
            int answer = 1;
            do{
                answer *= number;
                number--;
            }while(number > 0);
            return answer;
        }
    };

    public abstract int calculate(int number);

}
