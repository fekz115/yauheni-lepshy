package task.algorithms;

public enum FibonacciSequence {

    FOR {
        @Override
        public int calculate(int number) {
            int answer = 1;
            int previous = 1;
            System.out.print(answer + " ");
            for(int i = 1; i < number; i++){
                int temp = answer;
                answer += previous;
                previous = temp;
                System.out.print(answer + " ");
            }
            System.out.println();
            return answer;
        }
    }, WHILE{
        @Override
        public int calculate(int number) {
            int answer = 1;
            int previous = 0;
            while(number > 0){
                number--;
                int temp = answer;
                answer += previous;
                previous = temp;
                System.out.print(answer + " ");
            }
            System.out.println();
            return answer;
        }
    }, DOWHILE{
        @Override
        public int calculate(int number) {
            int answer = 1;
            int previous = 0;
            do{
                number--;
                int temp = answer;
                answer += previous;
                previous = temp;
                System.out.print(answer + " ");
            }while(number > 0);
            System.out.println();
            return answer;
        }
    };

    public abstract int calculate(int number);

}
