package tests;

import org.junit.jupiter.api.Test;
import task.Main;
import task.algorithms.Factorial;

class FactorialTests {

    @Test
    void FactorialTest(){
        for(int i = 1; i < Main.MAX_INT_FOR_FACTORIAL; i++){
            FactorialTest(i);
        }
    }

    void FactorialTest(int testedValue) {
        int answer = Factorial(testedValue);
        int answerCalculatedByForLoop = Factorial.FOR.calculate(testedValue);
        int answerCalculatedByWhileLoop = Factorial.WHILE.calculate(testedValue);
        int answerCalculatedByDoWhileLoop = Factorial.DOWHILE.calculate(testedValue);
        System.out.println(answer + " " + answerCalculatedByForLoop + " " + answerCalculatedByWhileLoop + " " + answerCalculatedByDoWhileLoop);
        assert answer == answerCalculatedByForLoop;
        assert answer == answerCalculatedByWhileLoop;
        assert answer == answerCalculatedByDoWhileLoop;
    }


    private static int Factorial(int number){
        return number == 0 ? 1 : number * Factorial(number - 1);
    }

}
