package tests;

import org.junit.jupiter.api.Test;
import task.Main;
import task.algorithms.FibonacciSequence;

class FibonacciSequenceTests {

    @Test
    void fibonacciSequenceTest(){
        for(int i = 1; i < Main.MAX_INT_FOR_FIBONACCI; i++){
            fibonacciSequenceTest(i);
        }
    }

    void fibonacciSequenceTest(int testedValue) {
        int answer = fibonacciSequence(1, 0, testedValue);
        int answerCalculatedByForLoop = FibonacciSequence.FOR.calculate(testedValue);
        int answerCalculatedByWhileLoop = FibonacciSequence.WHILE.calculate(testedValue);
        int answerCalculatedByDoWhileLoop = FibonacciSequence.DOWHILE.calculate(testedValue);
        System.out.println(answer + " " + answerCalculatedByForLoop + " " + answerCalculatedByWhileLoop + " " + answerCalculatedByDoWhileLoop);
        assert answer == answerCalculatedByForLoop;
        assert answer == answerCalculatedByWhileLoop;
        assert answer == answerCalculatedByDoWhileLoop;
    }

    private static int fibonacciSequence(int a, int b, int n){
        return n > 0 ? fibonacciSequence(a+b, a, n-1) : a;
    }

}