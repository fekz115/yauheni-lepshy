package main.java;

import main.java.domain.Polygon;
import main.java.domain.Vector;

/**
 * This class is intended for running program. It contains program entry point
 */
public class Runner {

    public static void main(String[] args) {
        Polygon polygon = new Polygon(new Vector(0, 0), new Vector(1, 0), new Vector(1, 1), new Vector(0, 1));
        System.out.println(polygon);
        polygon.move(new Vector(2, -4));
        System.out.println(polygon);
    }

}
