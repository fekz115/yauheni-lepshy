package main.java.domain;

public class Circle extends Shape implements Closed {

    /**
     * This field contains center of current circle
     */
    private Vector center;

    /**
     * This field contains radius of circle
     */
    private double r;

    /**
     * Constructor that creates circle with defined center coords and radius
     * @param center This parameter contains vector with coords of circle center
     * @param r This parameter contains radius of circle
     */
    public Circle(Vector center, double r) {
        this.center = center;
        this.r = r;
    }

    /**
     * @see Closed#getSquare()
     */
    @Override
    public double getSquare() {
        return Math.PI * r * r;
    }

    /**
     * @see Shape#getLength()
     */
    @Override
    double getLength() {
        return 2 * Math.PI * r;
    }

    /**
     * @see Shape#getCenter()
     */
    @Override
    Vector getCenter() {
        return center;
    }

    /**
     * @see Moveable#move(Vector)
     * @param shift Vector that describes new relative position of shape
     */
    @Override
    public void move(Vector shift) {
        center = center.plus(shift);
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Circle{").append("center=").append(center).append(", r=").append(r).append('}').toString();
    }
}
