package main.java.domain;

/**
 * This interface describes logic of closed shapes. Closed shapes has square
 */
public interface Closed {

    /**
     * This method is used to get square of shape
     * @return This returns value of shape square
     */
    double getSquare();

}
