package main.java.domain;

/**
 * This class describes model of line
 */
public class Line extends Shape {

    /**
     * Vector start is vector that contains the coords of line beginning
     * Vector finish contains the coords of line ending
     */
    private Vector start;
    private Vector finish;

    /**
     * This constructor creates line with defined vectors of beginning and ending
     * @param start Vector with coords of line beginning
     * @param finish Vector with coords of line ending
     */
    public Line(Vector start, Vector finish) {
        this.start = start;
        this.finish = finish;
    }

    /**
     * @see Shape#getLength()
     */
    @Override
    double getLength() {
        return finish.minus(start).getAbs();
    }

    /**
     * @see Shape#getCenter()
     */
    @Override
    Vector getCenter() {
        return finish.minus(start).mul(0.5);
    }

    /**
     * @see Moveable#move(Vector)
     * @param shift Vector that describes new relative position of shape
     */
    @Override
    public void move(Vector shift) {
        start = start.plus(shift);
        finish = finish.plus(shift);
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Line{").append(start).append(finish).append('}').toString();
    }
}
