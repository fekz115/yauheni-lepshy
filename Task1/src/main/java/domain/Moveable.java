package main.java.domain;

/**
 * This interface describes model of shapes that can be shifted
 */
public interface Moveable {

    /**
     * This interface used to move shape
     * @param shift Vector that describes new relative position of shape
     */
    void move(Vector shift);

}
