package main.java.domain;

import java.util.Arrays;

/**
 * This class describes model of Polygon
 * @author fekz115
 * @see main.java.domain.Shape
 */
public class Polygon extends Shape implements Closed {

    /**
     * This field contains array of polygon vertices
     */
    private Vector[] vertices;

    /**
     * This constructor creates polygon by array of vertices
     * @param vertices Array of polygon vertices
     */
    public Polygon(Vector...vertices) {
        this.vertices = vertices;
    }

    /**
     * @see main.java.domain.Shape#getLength()
     */
    @Override
    public double getLength() {
        double length = 0;
        for(int i = 0; i < vertices.length; i++){
            length += vertices[i].minus(vertices[(i+1)%vertices.length]).getAbs();
        }
        return length;
    }


    /**
     * @see main.java.domain.Shape#getCenter()
     * @return This returns center of polygon
     */
    @Override
    Vector getCenter() {
        Vector center = new Vector(0, 0);
        for(Vector v : vertices){
            center.plus(v);
        }
        return center.mul(1/vertices.length);
    }

    /**
     * @see main.java.domain.Closed#getSquare()
     * @return This returns square of polygon
     */
    @Override
    public double getSquare() {
        double square = 0;
        for(int i = 0; i < vertices.length-1; i++){
            square += vertices[i].getX() * vertices[i+1].getY() - vertices[i].getY() * vertices[i+1].getX();
        }
        return square/2;
    }

    /**
     * @see Moveable#move(Vector)
     * @param shift Vector that describes new relative position of shape
     */
    @Override
    public void move(Vector shift) {
        Arrays.setAll(vertices, i -> vertices[i].plus(shift));
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Polygon{").append("vertices=").append(Arrays.toString(vertices)).append('}').toString();
    }
}
