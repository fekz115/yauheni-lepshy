package main.java.domain;

/**
 * This class describes model of Shape
 * @author fekz115
 */
public abstract class Shape implements Moveable {

    /**
     * This method calculates the perimeter of shape
     * @return Returned value is perimeter of shape
     */
    abstract double getLength();

    /**
     * This method calculates central point of shape
     * @return Returned value is Vector with coords of this shape
     */
    abstract Vector getCenter();

}
