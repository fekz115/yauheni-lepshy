package main.java.domain;

/**
 * This class describes model of vector or vertex
 */
public class Vector {

    /**
     * This fields is vector coords
     */
    private double x, y;

    /**
     * Constructor that creates vector by coords
     * @param x x coordinate
     * @param y y coordinate
     */
    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Property for getting x-coordinate
     * @return x value
     */
    public double getX() {
        return x;
    }

    /**
     * Property for getting y-coordinate
     * @return y value
     */
    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("(").append(x).append("; ").append(y).append(')').toString();
    }

    /**
     * This method implements vector adding
     * @param b The second vector of sum
     * @return This returns new vector that is result of adding this vector and vector b
     */
    public Vector plus(Vector b){
        return new Vector(this.x + b.x, this.y + b.y);
    }

    /**
     * This is static method for vector sum
     * @param a First vector of sum
     * @param b Second vector of sum
     * @return This returns the sum of vectors a and b
     */
    public Vector sum(Vector a, Vector b){
        return a.plus(b);
    }

    /**
     * This method implements vector difference
     * @param b The second vector of difference
     * @return This returns new vector that is result of difference this vector and vector b
     */
    public Vector minus(Vector b){
        return new Vector(this.x - b.x, this.y - b.y);
    }

    /**
     * This is static method for vector difference
     * @param a First vector of difference
     * @param b Second vector of difference
     * @return This returns the difference of vectors a and b
     */
    public Vector sub(Vector a, Vector b){
        return a.minus(b);
    }

    /**
     * This method is used to get vector absolute value
     * @return This returns vector absolute value
     */
    public double getAbs(){
        return Math.sqrt(x*x + y*y);
    }

    /**
     * This method returns result of multiplication of this vector and value
     * @param a value that used in multiplication
     * @return This returns result of multiplication of this vector and value a
     */
    public Vector mul(double a){
        return new Vector(x * a, y * a);
    }
}
