package domain;

public class PlasticWindow extends Window {

    public PlasticWindow(int width, int height, String factory) {
        super(width, height, factory);
    }

    @Override
    public String toString() {
        return "PlasticWindow{" +
                "width=" + width +
                ", height=" + height +
                ", factory='" + factory + '\'' +
                '}';
    }
}
