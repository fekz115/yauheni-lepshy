package domain;

public class SpecialWindow extends Window {

    public SpecialWindow(int width, int height, String factory) {
        super(width, height, factory);
    }

    @Override
    public String toString() {
        return "SpecialWindow{" +
                "width=" + width +
                ", height=" + height +
                ", factory='" + factory + '\'' +
                '}';
    }

}
