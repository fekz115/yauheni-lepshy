package domain;

public abstract class Window {

    protected int width, height;
    protected String factory;

    public Window(int width, int height, String factory) {
        this.width = width;
        this.height = height;
        this.factory = factory;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getFactory() {
        return factory;
    }
}
