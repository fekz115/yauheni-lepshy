package domain;

public class WoodenWindow extends Window {

    public WoodenWindow(int width, int height, String factory) {
        super(width, height, factory);
    }

    @Override
    public String toString() {
        return "WoodenWindow{" +
                "width=" + width +
                ", height=" + height +
                ", factory='" + factory + '\'' +
                '}';
    }

}
