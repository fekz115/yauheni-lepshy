package factory;

import domain.PlasticWindow;
import domain.SpecialWindow;
import domain.Window;
import domain.WoodenWindow;

public interface AbstractWindowFactory {

    PlasticWindow createPlasticWindow(int width, int height);
    WoodenWindow createWoodenWindow(int width, int height);
    SpecialWindow createSpecialWindow(int width, int height);

}
