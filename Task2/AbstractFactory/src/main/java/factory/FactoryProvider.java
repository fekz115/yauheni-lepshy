package factory;

public enum FactoryProvider {

    Homel {
        public AbstractWindowFactory getFactory() {
            System.out.println("Created Homel window factory");
            return new HomelWindowFactory();
        }
    }, Minsk {
        public AbstractWindowFactory getFactory() {
            System.out.println("Created Minsk window factory");
            return new MinskWindowFactory();
        }
    };

    public abstract AbstractWindowFactory getFactory();

}
