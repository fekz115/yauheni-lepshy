package factory;

import domain.PlasticWindow;
import domain.SpecialWindow;
import domain.WoodenWindow;

public class HomelWindowFactory implements AbstractWindowFactory {

    private static final String city = "Homel";

    public PlasticWindow createPlasticWindow(int width, int height) {
        System.out.println("Created plastic window by Homel window factory");
        return new PlasticWindow(width, height, city);
    }

    public WoodenWindow createWoodenWindow(int width, int height) {
        System.out.println("Created wooden window by Homel window factory");
        return new WoodenWindow(width, height, city);
    }

    public SpecialWindow createSpecialWindow(int width, int height) {
        System.out.println("Created special window by Homel window factory");
        return new SpecialWindow(width, height, city);
    }

}