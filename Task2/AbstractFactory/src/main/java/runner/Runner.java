package runner;

import domain.Window;
import factory.AbstractWindowFactory;
import factory.FactoryProvider;

public class Runner {

    public static void main(String[] args) {

        Window[] windows = new Window[4];

        FactoryProvider factoryProvider = FactoryProvider.Homel;
        AbstractWindowFactory factory = factoryProvider.getFactory();

        windows[0] = factory.createPlasticWindow(100, 100);
        windows[1] = factory.createSpecialWindow(200, 150);

        factoryProvider = FactoryProvider.Minsk;
        factory = factoryProvider.getFactory();

        windows[2] = factory.createWoodenWindow(240, 300);
        windows[3] = factory.createPlasticWindow(500, 300);

        for (Window window : windows) {
            System.out.println(window);
        }

    }

}