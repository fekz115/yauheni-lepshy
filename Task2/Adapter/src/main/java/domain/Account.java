package domain;

import java.util.ArrayList;

public class Account {

    private String name;
    private int id;
    private ArrayList<Integer> tickets = new ArrayList<>();

    public Account(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void addTicket(int ticketId){
        tickets.add(ticketId);
    }

    public ArrayList<Integer> getTickets() {
        return tickets;
    }

}
