package runner;

import domain.Account;
import service.NewTicketServiceAdapter;
import service.StandardNewTicketService;
import service.TicketService;

public class Runner {

    public static void main(String[] args) {

        TicketService service = new NewTicketServiceAdapter(new StandardNewTicketService());

        Account firstAccount = new Account(1, "John");
        Account secondAccount = new Account(2, "Nick");

        firstAccount.addTicket(service.bookTicket());
        secondAccount.addTicket(service.bookTicket());
        firstAccount.addTicket(service.bookTicket());



    }

}
