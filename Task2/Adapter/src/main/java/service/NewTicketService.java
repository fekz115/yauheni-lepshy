package service;

import domain.Account;

/**
 * This interface describes logic of new ticket API
 */
public interface NewTicketService {

    /**
     * This method is used to calculate summary price of all tickets of account
     * @param user This param contains Account of calculated user
     * @return This returns summary price of tickets of Account user
     */
    int calculate(Account user);

    /**
     * This method is used to check if ticket exist
     * @param ticketId This param contains id of checked ticket
     * @return This returns true if ticket exist
     */
    boolean validateTicket(int ticketId);

    /**
     * This method creates new ticket
     * @return This returns id of new ticket
     */
    int book();

}
