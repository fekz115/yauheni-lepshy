package service;

import domain.Account;

/**
 * This class implements adapter to use NewTicketService as TicketService
 * @see service.TicketService
 * @see service.NewTicketService
 */
public class NewTicketServiceAdapter implements TicketService {

    private NewTicketService service;

    public NewTicketServiceAdapter(NewTicketService service) {
        this.service = service;
    }

    @Override
    public int getPrice(int ticketId) {
        Account tempAccount = new Account( 0, "");
        tempAccount.addTicket(ticketId);
        return service.calculate(tempAccount);
    }

    @Override
    public boolean validate(int ticketId) {
        return service.validateTicket(ticketId);
    }

    @Override
    public int bookTicket() {
        return service.book();
    }
}
