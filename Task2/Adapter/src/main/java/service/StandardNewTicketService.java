package service;

import domain.Account;
import domain.Ticket;

import java.util.ArrayList;
import java.util.Optional;

/**
 * This class is standard implementation of NewTicketService
 * @see service.NewTicketService
 */
public class StandardNewTicketService implements NewTicketService {

    private static int lastId = 0;
    private static ArrayList<Ticket> tickets = new ArrayList<>();

    public int calculate(Account user) {
        int answer = 0;
        for(int tempId : user.getTickets()){
            if(validateTicket(tempId)){
                Optional<Ticket> temp = tickets.stream().filter(i -> tempId == i.getId()).findFirst();
                if(temp.isPresent()){
                    answer += temp.get().getPrice();
                }
            }
        }
        return answer;
    }

    public boolean validateTicket(int ticketId) {
        long ticketsWithThisIdCount = tickets.stream().filter(i -> ticketId == i.getId()).count();
        return ticketsWithThisIdCount != 0;
    }

    public int book() {
        tickets.add(createTicket());
        return ++lastId;
    }

    private Ticket createTicket(){
        return new Ticket(lastId, (int)(Math.random()*1000));
    }
}
