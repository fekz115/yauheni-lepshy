package service;

/**
 * This interface describes API of old ticket service
 */
public interface TicketService {

    /**
     * This method is used to get ticket price
     * @param ticketId Ticket id
     * @return This returns cost of ticket
     */
    int getPrice(int ticketId);

    /**
     * This method is used to check if ticket exist
     * @param ticketId Id of checked ticket
     * @return This returns true if ticket exist
     */
    boolean validate(int ticketId);

    /**
     * This method creates new ticket
     * @return This returns id of new ticket
     */
    int bookTicket();

}
