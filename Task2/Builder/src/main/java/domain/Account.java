package domain;

import java.util.Date;

public class Account {

    private int id;
    private String firstName;
    private String lastName;
    private String login;
    private Gender gender;
    private Date birthDate;

    private Account(){}

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public Gender getGender() {
        return gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", gender=" + gender +
                ", birthDate=" + birthDate +
                '}';
    }

    public static AccountBuilder createBuilder(){
        return new Account().new AccountBuilder();
    }

    public class AccountBuilder{

        private AccountBuilder() {}

        public AccountBuilder setId(int id) {
            Account.this.id = id;
            return this;
        }

        public AccountBuilder setFirstName(String firstName) {
            Account.this.firstName = firstName;
            return this;
        }

        public AccountBuilder setLastName(String lastName) {
            Account.this.lastName = lastName;
            return this;
        }

        public AccountBuilder setLogin(String login) {
            Account.this.login = login;
            return this;
        }

        public AccountBuilder setGender(Gender gender) {
            Account.this.gender = gender;
            return this;
        }

        public AccountBuilder setBirthDate(Date birthDate) {
            Account.this.birthDate = birthDate;
            return this;
        }

        public Account build(){
            return Account.this;
        }
        
    }
}
