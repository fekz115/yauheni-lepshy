package runner;

import domain.Account;
import domain.Gender;

import java.util.Date;

public class Runner {

    public static void main(String[] args) {

        Account.AccountBuilder builder = Account.createBuilder();

        System.out.println("First account");

        builder.setId(1).setFirstName("Nick").setGender(Gender.Male);

        Account nick = builder.build();

        System.out.println(nick);

        System.out.println("Second account");

        builder = Account.createBuilder();

        builder.setId(2).setFirstName("Julia").setLastName("Michaels").setGender(Gender.Female).setBirthDate(new Date()).setLogin("User");

        Account julia = builder.build();

        System.out.println(julia);

    }

}
