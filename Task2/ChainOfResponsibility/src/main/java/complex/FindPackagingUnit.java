package complex;

import domain.Product;

public class FindPackagingUnit extends Unit {

    private boolean isPackagingPresent = true;

    @Override
    public boolean check(String product) {
        if(isPackagingPresent){
            return checkNext(product);
        } else{
            System.out.println("There is no packaging in storage");
            return false;
        }
    }
}
