package complex;

import domain.Product;

public class FindProductUnit extends Unit {

    private boolean isProductPresent = true;

    @Override
    public boolean check(String product) {
        if(isProductPresent){
            return checkNext(product);
        } else{
            System.out.println("The product didn't found in storage");
            return false;
        }
    }

}
