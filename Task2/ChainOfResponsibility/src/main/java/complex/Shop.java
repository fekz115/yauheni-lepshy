package complex;

import domain.Product;

public class Shop {

    public void makePayment(Product product){
        product.setPaymentExecuted(true);
    }

}
