package complex;

import domain.Product;

public class Storage {

    public Product getProduct(String name){
        return new Product(name);
    }

    public void pack(Product product){
        product.setPackaging(true);
    }

}
