package complex;

import domain.Product;

public class StorageCheckUnit  extends Unit {

    private boolean isWorking = true;

    @Override
    public boolean check(String product) {
        if(isWorking){
            return checkNext(product);
        } else {
            System.out.println("Storage doesn't work now");
            return false;
        }
    }
}
