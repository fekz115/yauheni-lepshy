package complex;

import domain.Product;

public class TransporterCheckUnit extends Unit {

    private boolean isTransporterWorking = true;

    @Override
    public boolean check(String product) {
        if(isTransporterWorking){
            return checkNext(product);
        } else{
            System.out.println("Transporter doesn't work now");
            return false;
        }
    }
}
