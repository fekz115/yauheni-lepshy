package complex;

public abstract class Unit {

    private Unit nextUnit;

    public Unit addUnit(Unit next){
        nextUnit = next;
        return next;
    }

    public abstract boolean check(String product);

    protected boolean checkNext(String product){
        if(nextUnit == null){
            return true;
        } else {
            return nextUnit.check(product);
        }
    }

}
