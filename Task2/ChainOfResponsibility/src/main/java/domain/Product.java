package domain;

public class Product {

    private String name;
    private boolean packaging = false;
    private boolean paymentExecuted = false;

    public Product(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasPackaging() {
        return packaging;
    }

    public void setPackaging(boolean packaging) {
        this.packaging = packaging;
    }

    public boolean isPaymentExecuted() {
        return paymentExecuted;
    }

    public void setPaymentExecuted(boolean paymentExecuted) {
        this.paymentExecuted = paymentExecuted;
    }

    @Override
    public String toString() {
        return "domain.Product{" +
                "name='" + name + '\'' +
                ", packaging=" + packaging +
                ", paymentExecuted=" + paymentExecuted +
                '}';
    }

}
