package runner;

import complex.*;
import domain.Product;
import facade.ProductDeliveryFacade;

public class Runner {

    private static Unit createChain(){
        return new StorageCheckUnit().addUnit(new FindProductUnit()).addUnit(new FindPackagingUnit()).addUnit(new TransporterCheckUnit());
    }

    public static void main(String[] args) {
        Unit chain = createChain();
        String requiredProduct = "Some Product";
        System.out.println("Required product: " + requiredProduct);
        if(chain.check(requiredProduct)) {
            Product product = ProductDeliveryFacade.order(requiredProduct);
            System.out.println(product);
        } else {
            System.out.println("Something went wrong");
        }
    }

}
