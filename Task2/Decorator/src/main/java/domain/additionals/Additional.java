package domain.additionals;

import domain.drinks.Drink;
import domain.drinks.Tee;

public class Additional implements Drink {

    private Drink drink;

    public Additional(Drink drink) {
        this.drink = drink;
    }

    @Override
    public int calculateCost() {
        return drink.calculateCost();
    }

    @Override
    public String toString() {
        return drink.toString();
    }
}
