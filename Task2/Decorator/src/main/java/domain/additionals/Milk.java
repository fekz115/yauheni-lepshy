package domain.additionals;

import domain.drinks.Drink;
import domain.drinks.Tee;

public class Milk extends Additional {

    private final static int cost = 5;

    public Milk(Drink drink) {
        super(drink);
    }

    @Override
    public int calculateCost() {
        return super.calculateCost() + cost;
    }

    @Override
    public String toString() {
        return super.toString() + " with milk";
    }

}
