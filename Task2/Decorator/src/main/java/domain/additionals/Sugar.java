package domain.additionals;

import domain.drinks.Drink;

public class Sugar extends Additional {

    private final static int cost = 3;

    public Sugar(Drink drink) {
        super(drink);
    }

    @Override
    public int calculateCost() {
        return super.calculateCost() + cost;
    }

    @Override
    public String toString() {
        return super.toString() + " with sugar";
    }
}
