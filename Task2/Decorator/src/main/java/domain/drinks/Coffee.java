package domain.drinks;

public class Coffee implements Drink {

    @Override
    public int calculateCost() {
        return 10;
    }

    @Override
    public String toString() {
        return "Coffee";
    }
}
