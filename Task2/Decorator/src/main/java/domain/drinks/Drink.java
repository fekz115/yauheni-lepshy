package domain.drinks;

public interface Drink {

    int calculateCost();

}