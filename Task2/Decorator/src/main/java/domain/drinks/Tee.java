package domain.drinks;

public class Tee implements Drink {
    @Override
    public int calculateCost() {
        return 5;
    }

    @Override
    public String toString() {
        return "Tee";
    }

}
