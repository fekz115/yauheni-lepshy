package runner;

import domain.additionals.Additional;
import domain.additionals.Milk;
import domain.additionals.Sugar;
import domain.drinks.Coffee;
import domain.drinks.Tee;

public class Runner {

    public static void main(String[] args) {

        System.out.println("Creating Tee with milk");
        Additional teeWithMilk = new Additional(new Milk(new Tee()));
        System.out.println(teeWithMilk);
        System.out.println("Cost of tee with milk: " + teeWithMilk.calculateCost());

        System.out.println("Creating coffee with sugar and milk");
        Additional coffeeWithSugarAndMilk = new Additional(new Sugar(new Milk(new Coffee())));
        System.out.println(coffeeWithSugarAndMilk);
        System.out.println("Cost of coffee with sugar and milk: " + coffeeWithSugarAndMilk.calculateCost());

    }

}
