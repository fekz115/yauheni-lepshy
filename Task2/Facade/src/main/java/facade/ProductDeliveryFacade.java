package facade;

import complex.Shop;
import complex.Storage;
import domain.Product;

public class ProductDeliveryFacade {

    private static Storage storage = new Storage();
    private static Shop shop = new Shop();

    public static Product order(String name){
        Product product = storage.getProduct(name);
        System.out.println("Product received from storage");
        storage.pack(product);
        System.out.println("Product packed");
        System.out.println("Product delivered to shop");
        shop.makePayment(product);
        System.out.println("Product payment was made");
        System.out.println("Product ready to deliver to customer");
        return product;
    }

}
