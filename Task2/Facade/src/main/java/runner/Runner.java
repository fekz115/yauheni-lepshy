package runner;

import domain.Product;
import facade.ProductDeliveryFacade;

public class Runner {

    public static void main(String[] args) {

        String requiredProduct = "Some Product";

        System.out.println("Required product: " + requiredProduct);

        Product product = ProductDeliveryFacade.order(requiredProduct);

        System.out.println(product);

    }

}
