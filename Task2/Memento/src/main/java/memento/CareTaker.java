package memento;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CareTaker {

    private List<Memento> mementoList = new ArrayList<>();

    public CareTaker(int tempNumber){
        mementoList.add(new Memento(tempNumber));
    }

    public CareTaker mul(int number){
        int last = getLast().getTempNumber();
        last *= number;
        mementoList.add(new Memento(last));
        return this;
    }

    public CareTaker div(int number){
        int last = getLast().getTempNumber();
        try {
            last /= number;
            mementoList.add(new Memento(last));
        }catch (ArithmeticException e){

        }
        return this;
    }

    private Memento getLast(){
        return mementoList.get(getLastIndex());
    }

    private int getLastIndex(){
        return mementoList.size()-1;
    }

    public CareTaker cancel(){
        if(!mementoList.isEmpty()){
            mementoList.remove(getLastIndex());
        }
        return this;
    }

    @Override
    public String toString() {
        return "CareTaker{" +
                "mementoList=" + Arrays.toString(mementoList.toArray()) +
                '}';
    }
}
