package memento;

public class Memento {

    private int tempNumber;

    public Memento(int tempNumber) {
        this.tempNumber = tempNumber;
    }

    public int getTempNumber() {
        return tempNumber;
    }

    @Override
    public String toString() {
        return "Memento{" +
                "tempNumber=" + tempNumber +
                '}';
    }
}
