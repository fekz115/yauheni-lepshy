package runner;

import memento.CareTaker;

public class Runner {

    public static void main(String[] args) {

        CareTaker taker = new CareTaker(1);

        taker.mul(5).mul(20).div(3);

        System.out.println(taker);

        taker.div(0);

        System.out.println(taker);

        taker.cancel().cancel();

        System.out.println(taker);

    }

}
