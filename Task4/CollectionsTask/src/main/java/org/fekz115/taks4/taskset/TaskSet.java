package org.fekz115.taks4.taskset;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskSet<T> extends HashSet<T> {

    public static<E> Set<E> union(Set<E> set1, Set<E> set2){
        return Stream.concat(set1.stream(), set2.stream())
                .collect(Collectors.toSet());
    }

    public Set<T> union(Set<T> set) {
        return union(this, set);
    }

    public static<E> Set<E> intersection(Set<E> set1, Set<E> set2){
        return set1.stream()
                .filter(set2::contains)
                .collect(Collectors.toSet());
    }

    public Set<T> intersection(Set<T> set) {
        return intersection(this, set);
    }

    public static<E> Set<E> difference(Set<E> set1, Set<E> set2){
        return set1.stream()
                .filter(x -> !set2.contains(x))
                .collect(Collectors.toSet());
    }

    public Set<T> difference(Set<T> set) {
        return difference(this, set);
    }

    public static<E> Set<E> symmetricDifference(Set<E> set1, Set<E> set2){
        return union(difference(set1, set2), difference(set2, set1));
    }

    public Set<T> symmetricDifference(Set<T> set) {
        return symmetricDifference(this, set);
    }

}
