package org.fekz115.task4.taskset;

import org.fekz115.taks4.taskset.TaskSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Set;

public class Tests {

    @Test
    public void testUnion(){

        TaskSet<Integer> set1 = new TaskSet<>();
        Arrays.stream(new int[]{1, 2, 4})
                .forEach(set1::add);

        TaskSet<Integer> set2 = new TaskSet<>();
        Arrays.stream(new int[]{3, 4, 5, 6})
                .forEach(set2::add);

        Set<Integer> result = set1.union(set2);

        printSet(result);

        Assertions.assertArrayEquals(result.toArray(), new Object[]{1, 2, 3, 4, 5, 6});

    }


    @Test
    public void testIntersection(){

        TaskSet<Integer> set1 = new TaskSet<>();
        Arrays.stream(new int[]{1, 2, 4})
                .forEach(set1::add);

        TaskSet<Integer> set2 = new TaskSet<>();
        Arrays.stream(new int[]{3, 4, 5, 2})
                .forEach(set2::add);

        Set<Integer> result = set1.intersection(set2);

        printSet(result);

        Assertions.assertArrayEquals(result.toArray(), new Object[]{2, 4});

    }

    @Test
    public void testDifference(){

        TaskSet<Integer> set1 = new TaskSet<>();
        Arrays.stream(new int[]{1, 2, 3, 4})
                .forEach(set1::add);

        TaskSet<Integer> set2 = new TaskSet<>();
        Arrays.stream(new int[]{3, 4, 5})
                .forEach(set2::add);

        Set<Integer> result = set1.difference(set2);

        printSet(result);

        Assertions.assertArrayEquals(result.toArray(), new Object[]{1, 2});

    }

    @Test
    public void testSymmetricDifference(){

        TaskSet<Integer> set1 = new TaskSet<>();
        Arrays.stream(new int[]{1, 2, 3})
                .forEach(set1::add);

        TaskSet<Integer> set2 = new TaskSet<>();
        Arrays.stream(new int[]{3, 4})
                .forEach(set2::add);

        Set<Integer> result = set1.symmetricDifference(set2);

        printSet(result);

        Assertions.assertArrayEquals(result.toArray(), new Object[]{1, 2, 4});

    }

    private void printSet(Set set) {
        System.out.println(Arrays.toString(set.toArray()));
    }
    
}
