package org.fekz115.task4.validationtask;

import org.fekz115.task4.validationtask.domain.Person;
import org.fekz115.task4.validationtask.validator.*;


public class Runner {

    public static final int MIN_AGE = 16;
    public static final int MIN_HEIGHT = 160;

    public static final int[][] intervals = {
            {1, 10},
            {15, 30}
    };

    public static final String matcherPattern = "^[A-Z].{0,14}$";

    public static void main(String[] args) {

        Validator intValidator = new IntegerValidator(intervals);
        Validator stringValidator = new StringValidator(matcherPattern);
        Validator personValidator = new PersonValidator(MIN_AGE, MIN_HEIGHT);

        ValidationSystem validationSystem = ValidationSystem.createBuilder()
                .addValidator(intValidator)
                .addValidator(stringValidator)
                .addValidator(personValidator)
                .build();

        try {
            validationSystem.validate(5);
            validationSystem.validate("Asadfasdf");
            validationSystem.validate(new Person(17, 189));
        } catch (ValidationFailedException e) {
            e.printStackTrace();
        }

    }

}
