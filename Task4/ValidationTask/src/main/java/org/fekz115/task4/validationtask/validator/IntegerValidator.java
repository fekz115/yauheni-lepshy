package org.fekz115.task4.validationtask.validator;

public class IntegerValidator implements Validator<Integer> {

    private int[][] intervals;

    public IntegerValidator(int[][] intervals) {
        this.intervals = intervals;
    }

    @Override
    public void validate(Integer value) throws ValidationFailedException {
        for (int[] interval : intervals) {
            if(checkInterval(value, interval)){
                return;
            }
        }
        throw new ValidationFailedException();
    }

    @Override
    public Class<Integer> getMyType() {
        return Integer.class;
    }

    private static boolean checkInterval(int value, int[] interval){
        return (value >= interval[0] && value <= interval[1]);
    }

}
