package org.fekz115.task4.validationtask.validator;

import org.fekz115.task4.validationtask.domain.Person;

public class PersonValidator implements Validator<Person> {

    private int minAge;
    private int minHeight;

    public PersonValidator(int minAge, int minHeight) {
        this.minAge = minAge;
        this.minHeight = minHeight;
    }

    @Override
    public void validate(Person value) throws ValidationFailedException {
        if(!checkAge(value) || !checkHeight(value)){
            throw new ValidationFailedException();
        }
    }

    @Override
    public Class<Person> getMyType() {
        return Person.class;
    }

    private boolean checkAge(Person person){
        return person.getAge() > minAge;
    }

    private boolean checkHeight(Person person){
        return person.getHeight() > minHeight;
    }
}
