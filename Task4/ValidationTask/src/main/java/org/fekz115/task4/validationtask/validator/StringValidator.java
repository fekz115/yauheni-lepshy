package org.fekz115.task4.validationtask.validator;

import java.util.regex.Pattern;

public class StringValidator implements Validator<String> {

    private Pattern pattern;

    private static StringValidator instance = null;

    public StringValidator(String matcherPattern) {
        pattern = Pattern.compile(matcherPattern);
    }

    @Override
    public void validate(String value) throws ValidationFailedException {
        if(!pattern.matcher(value).matches()){
            throw new ValidationFailedException();
        }
    }

    @Override
    public Class<String> getMyType() {
        return String.class;
    }

}
