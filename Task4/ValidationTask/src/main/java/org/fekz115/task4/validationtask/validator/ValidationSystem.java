package org.fekz115.task4.validationtask.validator;

import java.util.HashMap;
import java.util.Map;

public class ValidationSystem {

    private final boolean defaultValue = true;

    private ValidationSystem(){}

    private Map<Class<?>, Validator<?>> typesMap = new HashMap<>();

    public static Builder createBuilder(){
        return new ValidationSystem().new Builder();
    }

    @SuppressWarnings("unchecked")
    public void validate(Object object) throws ValidationFailedException {
        Validator validator = typesMap.get(object.getClass());
        if(validator != null){
            validator.validate(object);
        } else {
            throw new ValidationFailedException();
        }
    }

    public class Builder {

        private Builder(){}

        public Builder addValidator(Validator validator){
            ValidationSystem.this.typesMap.put(validator.getMyType(), validator);
            return this;
        }

        public ValidationSystem build(){
            return ValidationSystem.this;
        }
    }

}
