package org.fekz115.task4.validationtask.validator;

public interface Validator<T> {

    void validate(T value) throws ValidationFailedException;

    Class<T> getMyType();

}
