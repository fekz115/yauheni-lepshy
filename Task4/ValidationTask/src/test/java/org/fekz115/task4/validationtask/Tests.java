package org.fekz115.task4.validationtask;

import org.fekz115.task4.validationtask.domain.Person;
import org.fekz115.task4.validationtask.validator.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Tests {

    private IntegerValidator integerValidator = new IntegerValidator(Runner.intervals);
    private StringValidator stringValidator = new StringValidator(Runner.matcherPattern);
    private PersonValidator personValidator = new PersonValidator(Runner.MIN_AGE, Runner.MIN_HEIGHT);

    private ValidationSystem validationSystem = ValidationSystem.createBuilder()
            .addValidator(integerValidator)
            .addValidator(stringValidator)
            .addValidator(personValidator)
            .build();

    @Test
    public void testValidateInt () throws ValidationFailedException {
        validationSystem.validate(1);
        validationSystem.validate(5);
        validationSystem.validate(10);
    }

    @Test
    public void testValidateIntFail() {
        Assertions.assertThrows(ValidationFailedException.class, () -> {
            validationSystem.validate(11);
        });
    }

    @Test
    public void testValidateStringFails() {
        Assertions.assertThrows(ValidationFailedException.class, () -> {
            validationSystem.validate("hello");
        });
    }

    @Test
    public void testNotRegisteredValidator() {
        ValidationSystem testSystem = ValidationSystem.createBuilder()
                .addValidator(integerValidator)
                .build();
        Assertions.assertThrows(ValidationFailedException.class, () -> {
            testSystem.validate("hello");
        });
    }

    @Test
    public void testValidateString() throws ValidationFailedException {
        validationSystem.validate("Asdfgh");
        validationSystem.validate("A");
        Assertions.assertThrows(ValidationFailedException.class, () -> {
            validationSystem.validate("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa");
        });
    }

    @Test
    public void testValidatePerson() throws ValidationFailedException {

        Person p1 = new Person(17, 180);
        Person p2 = new Person(14, 180);
        Person p3 = new Person(17, 150);
        Person p4 = new Person(11, 140);

        validationSystem.validate(p1);
        Assertions.assertThrows(ValidationFailedException.class, () -> {
            validationSystem.validate(p2);
        });
        Assertions.assertThrows(ValidationFailedException.class, () -> {
            validationSystem.validate(p3);
        });
        Assertions.assertThrows(ValidationFailedException.class, () -> {
            validationSystem.validate(p4);
        });

    }

}
