package org.fekz115.task5.tasks12;

import org.fekz115.task5.tasks12.city.City;
import org.fekz115.task5.tasks12.city.ThreadBasedCity;
import org.fekz115.task5.tasks12.cuncurrent.ConcurrentBankExchangePoint;
import org.fekz115.task5.tasks12.cuncurrent.ConcurrentCity;
import org.fekz115.task5.tasks12.cuncurrent.ConcurrentCustomExchangePoint;
import org.fekz115.task5.tasks12.domain.Tourist;
import org.fekz115.task5.tasks12.exchange.*;
import org.fekz115.task5.tasks12.presenter.ConsolePresenter;
import org.fekz115.task5.tasks12.presenter.LoggerPresenter;
import org.fekz115.task5.tasks12.presenter.Presenter;

import java.util.Scanner;

public class Runner {

    static City city;

    static Exchange exchange;

    static ExchangePoint[] exchangePoints;

    static int touristCount = 20;

    static Presenter presenter;

    public static void main(String[] args) {
        int n;
        Scanner in = new Scanner(System.in);
        do{
            System.out.println("Choose task [1/2]");
            n = in.nextInt();
        }while(n != 1 && n != 2);
        if(n == 1) {
            exchangePoints = new ExchangePoint[]{
                    new ThreadBasedBankExchangePoint(exchange, presenter, 20),
                    new ThreadBasedCustomExchangePoint(exchange, presenter)
            };
            city = new ThreadBasedCity(exchangePoints, exchange, presenter);
        } else {
            exchangePoints = new ExchangePoint[]{
                    new ConcurrentBankExchangePoint(exchange, presenter, 20),
                    new ConcurrentCustomExchangePoint(exchange, presenter)
            };
            city = new ConcurrentCity(exchangePoints, exchange, presenter);
        }
        exchange = new StandardExchange();
        presenter = new LoggerPresenter();
        city.openCity();
        for(int i = 0; i < touristCount; i++) {
            Tourist tourist = new Tourist("Name " + i);
            city.addTourist(tourist);
        }
        city.closeCity();
        if(city instanceof ThreadBasedCity){
            while(Thread.activeCount() > 2){}
        }
    }

}
