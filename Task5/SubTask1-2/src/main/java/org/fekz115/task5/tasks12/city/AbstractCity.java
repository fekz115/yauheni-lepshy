package org.fekz115.task5.tasks12.city;

import org.fekz115.task5.tasks12.domain.Tourist;
import org.fekz115.task5.tasks12.exchange.Exchange;
import org.fekz115.task5.tasks12.exchange.ExchangePoint;
import org.fekz115.task5.tasks12.presenter.Presenter;

import java.util.LinkedList;
import java.util.Queue;

public abstract class AbstractCity implements City {

    protected final ExchangePoint[] exchangePoints;
    private final Exchange exchange;
    protected final Presenter presenter;

    public AbstractCity(ExchangePoint[] exchangePoints, Exchange exchange, Presenter presenter) {
        this.exchangePoints = exchangePoints;
        this.exchange = exchange;
        this.presenter = presenter;
        for (ExchangePoint exchangePoint : exchangePoints) {
            exchangePoint.setListener(this::serveTouristActions);
            exchangePoint.open();
        }
    }

    private Queue<Tourist> touristQueue = new LinkedList<>();
    private int currentExchangePointIndex = 0;

    private boolean exit = false;

    private Tourist getCurrentTourist(){
        return touristQueue.poll();
    }

    @Override
    public void addTourist(Tourist tourist) {
        presenter.touristArrived(tourist);
        touristQueue.add(tourist);
    }

    protected void changeCourse(){
        double oldCourse = exchange.getCourse();
        exchange.changeCourse(((double)((int)(100 * Math.random())))/100 * 10 - 5);
        double newCourse = exchange.getCourse();
        presenter.courseChanged(oldCourse, newCourse);
    }

    protected void touristServeLoop(){
        while(!getExitCondition()) {
            ExchangePoint exchangePoint = getCurrentExchangePoint();
            Tourist tourist = getCurrentTourist();
            if(tourist != null) {
                exchangePoint.addTouristToQueue(tourist);
            }
        }
    }

    private void serveTouristActions(Tourist tourist, ExchangePoint exchangePoint){
        presenter.servingTourist(tourist);
        presenter.currentCourse(exchangePoint.getCourse());
        presenter.usedExchangePoint(exchangePoint);
    }

    private ExchangePoint getCurrentExchangePoint(){
        currentExchangePointIndex++;
        currentExchangePointIndex %= exchangePoints.length;
        return exchangePoints[currentExchangePointIndex];
    }


    @Override
    public void closeCity() {
        exit = true;
        for (ExchangePoint exchangePoint : exchangePoints) {
            exchangePoint.close();
        }
    }

    private boolean getExitCondition() {
        return exit && touristQueue.isEmpty();
    }

}
