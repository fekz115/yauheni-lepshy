package org.fekz115.task5.tasks12.city;

import org.fekz115.task5.tasks12.domain.Tourist;

public interface City {

    void addTourist(Tourist tourist);

    void openCity();

    void closeCity();

}
