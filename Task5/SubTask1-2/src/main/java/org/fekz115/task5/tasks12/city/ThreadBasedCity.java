package org.fekz115.task5.tasks12.city;

import org.fekz115.task5.tasks12.exchange.Exchange;
import org.fekz115.task5.tasks12.exchange.ExchangePoint;
import org.fekz115.task5.tasks12.presenter.Presenter;

public class ThreadBasedCity extends AbstractCity {

    private Thread mainLoopThread = new Thread(this::touristServeLoop);
    private Thread exchangeLoop = new Thread(this::courseChangeLoop);

    private boolean exitFromExchangeLoop() {
        for(ExchangePoint exchangePoint : exchangePoints){
            if(!exchangePoint.isClosed())return true;
        }
        return false;
    }

    private void courseChangeLoop() {
        try {
            while (!exitFromExchangeLoop()) {
                Thread.sleep(1000);
                changeCourse();
            }
        } catch (InterruptedException e) {
            presenter.showException(e);
        }
    }

    public ThreadBasedCity(ExchangePoint[] exchangePoints, Exchange exchange, Presenter presenter) {
        super(exchangePoints, exchange, presenter);
    }

    @Override
    public void openCity() {
        mainLoopThread.start();
        exchangeLoop.start();
    }

}
