package org.fekz115.task5.tasks12.cuncurrent;

import org.fekz115.task5.tasks12.exchange.BankExchangePoint;
import org.fekz115.task5.tasks12.exchange.Exchange;
import org.fekz115.task5.tasks12.presenter.Presenter;

public class ConcurrentBankExchangePoint extends BankExchangePoint {

    public ConcurrentBankExchangePoint(Exchange exchange, Presenter presenter, int n) {
        super(exchange, presenter, n);
    }

    @Override
    public void open() {
        ExecutorsProvider provider = ExecutorsProvider.getInstance();
        provider.scheduleTask(() -> {
            try {
                touristServingLoop();
            } catch (InterruptedException e) {
                presenter.showException(e);
            }
        });
    }
}
