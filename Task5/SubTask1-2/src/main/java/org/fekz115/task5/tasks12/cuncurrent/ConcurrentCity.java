package org.fekz115.task5.tasks12.cuncurrent;

import org.fekz115.task5.tasks12.city.AbstractCity;
import org.fekz115.task5.tasks12.exchange.Exchange;
import org.fekz115.task5.tasks12.exchange.ExchangePoint;
import org.fekz115.task5.tasks12.presenter.Presenter;

public class ConcurrentCity extends AbstractCity {

    public ConcurrentCity(ExchangePoint[] exchangePoints, Exchange exchange, Presenter presenter) {
        super(exchangePoints, exchange, presenter);
    }

    @Override
    public void openCity() {
        ExecutorsProvider provider = ExecutorsProvider.getInstance();
        provider.scheduleTask(this::touristServeLoop);
        provider.scheduleTask(this::changeCourse, 2000);
    }

    @Override
    public void closeCity(){
        super.closeCity();
        ExecutorsProvider provider = ExecutorsProvider.getInstance();
        provider.shutdown();
    }
}
