package org.fekz115.task5.tasks12.cuncurrent;

import org.fekz115.task5.tasks12.exchange.CustomExchangePoint;
import org.fekz115.task5.tasks12.exchange.Exchange;
import org.fekz115.task5.tasks12.presenter.Presenter;

public class ConcurrentCustomExchangePoint extends CustomExchangePoint {

    public ConcurrentCustomExchangePoint(Exchange exchange, Presenter presenter) {
        super(exchange, presenter);
    }

    @Override
    public void open() {
        ExecutorsProvider provider = ExecutorsProvider.getInstance();
        provider.scheduleTask(() -> {
            try {
                touristServingLoop();
            } catch (InterruptedException e) {
                presenter.showException(e);
            }
        });
    }
}
