package org.fekz115.task5.tasks12.cuncurrent;

import java.util.concurrent.*;

public class ExecutorsProvider {

    private static ExecutorsProvider provider = null;

    private ExecutorService executorService;
    private ScheduledExecutorService scheduledExecutorService;

    private ExecutorsProvider() {
        executorService = Executors.newCachedThreadPool();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    public static ExecutorsProvider getInstance() {
        if(provider == null){
            provider = new ExecutorsProvider();
        } else {
            if(provider.executorService.isTerminated() && provider.scheduledExecutorService.isTerminated()) {
                provider = new ExecutorsProvider();
            }
        }
        return provider;
    }

    public void scheduleTask(Runnable task, long period){
        scheduledExecutorService.scheduleAtFixedRate(task, 0, period, TimeUnit.MILLISECONDS);
    }

    public void scheduleTask(Runnable task) {
        executorService.submit(task);
    }

    public void shutdown(){
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            scheduledExecutorService.shutdown();
        }
    }

}
