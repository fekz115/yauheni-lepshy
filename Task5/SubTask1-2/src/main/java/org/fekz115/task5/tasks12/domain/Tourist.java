package org.fekz115.task5.tasks12.domain;

public class Tourist {

    private double money = 100 + (int)(Math.random() * 900);
    private String name;

    public Tourist(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tourist{" +
                "money=" + money +
                ", name='" + name + '\'' +
                '}';
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
