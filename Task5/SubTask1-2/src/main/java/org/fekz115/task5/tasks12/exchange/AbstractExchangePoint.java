package org.fekz115.task5.tasks12.exchange;

import org.fekz115.task5.tasks12.domain.Tourist;
import org.fekz115.task5.tasks12.presenter.Presenter;

import java.util.function.BiConsumer;

public abstract class AbstractExchangePoint implements ExchangePoint {

    Exchange exchange;

    BiConsumer<Tourist, ExchangePoint> onServe;

    protected boolean open = true;

    protected Presenter presenter;

    protected abstract long getTimeToServe();

    @Override
    public void setListener(BiConsumer<Tourist, ExchangePoint> onServe) {
        this.onServe = onServe;
    }

    public AbstractExchangePoint(Exchange exchange, Presenter presenter) {
        this.exchange = exchange;
        this.presenter = presenter;
    }

    protected void serveTourist(Tourist tourist) throws InterruptedException {
        Thread.sleep(getTimeToServe());
        onServe.accept(tourist, this);
        tourist.setMoney(getCourse() * tourist.getMoney());
    }

    @Override
    public void close() {
        open = false;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
