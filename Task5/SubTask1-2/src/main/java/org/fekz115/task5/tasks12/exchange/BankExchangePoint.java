package org.fekz115.task5.tasks12.exchange;

import org.fekz115.task5.tasks12.domain.Tourist;
import org.fekz115.task5.tasks12.presenter.Presenter;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public abstract class BankExchangePoint extends AbstractExchangePoint {

    private final static long timeToServe = 3000;

    private BlockingQueue<Tourist> queue;

    public BankExchangePoint(Exchange exchange, Presenter presenter, int n) {
        super(exchange, presenter);
        queue = new ArrayBlockingQueue<>(n);
    }

    @Override
    protected long getTimeToServe() {
        return timeToServe;
    }

    @Override
    public double getCourse() {
        return exchange.getCourse();
    }

    @Override
    public void addTouristToQueue(Tourist tourist) {
        queue.add(tourist);
    }

    protected void touristServingLoop() throws InterruptedException {
        while(!getExitCondition()){
            Tourist tourist = queue.poll();
            if(tourist != null) {
                serveTourist(tourist);
            }
        }
    }

    private boolean getExitCondition() {
        return !open && queue.isEmpty();
    }

    @Override
    public boolean isClosed() {
        return !getExitCondition();
    }

}
