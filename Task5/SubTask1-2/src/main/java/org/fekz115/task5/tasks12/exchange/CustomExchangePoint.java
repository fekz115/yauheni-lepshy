package org.fekz115.task5.tasks12.exchange;

import org.fekz115.task5.tasks12.domain.Tourist;
import org.fekz115.task5.tasks12.presenter.Presenter;

import java.util.LinkedList;
import java.util.Queue;

public abstract class CustomExchangePoint extends AbstractExchangePoint {

    private final static double discount = 0.95;
    private final static long timeToServe = 2000;
    private final Queue<Tourist> queue = new LinkedList<>();

    public CustomExchangePoint(Exchange exchange, Presenter presenter) {
        super(exchange, presenter);
    }

    @Override
    protected long getTimeToServe() {
        return timeToServe;
    }

    @Override
    public double getCourse() {
        return exchange.getCourse() * discount;
    }

    @Override
    public void addTouristToQueue(Tourist tourist) {
        queue.add(tourist);
    }

    protected void touristServingLoop() throws InterruptedException {
        while(!getExitCondition()){
            Tourist tourist = queue.poll();
            if(tourist != null) {
                serveTourist(tourist);
            }
        }
    }

    private boolean getExitCondition() {
        return !open && queue.isEmpty();
    }

    @Override
    public boolean isClosed() {
        return !getExitCondition();
    }

}
