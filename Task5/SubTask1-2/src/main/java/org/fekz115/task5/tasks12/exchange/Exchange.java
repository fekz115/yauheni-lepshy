package org.fekz115.task5.tasks12.exchange;

public interface Exchange {

    void changeCourse(double delta);

    double getCourse();

}
