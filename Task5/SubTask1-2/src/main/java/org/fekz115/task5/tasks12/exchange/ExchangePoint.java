package org.fekz115.task5.tasks12.exchange;

import org.fekz115.task5.tasks12.domain.Tourist;

import java.util.function.BiConsumer;

public interface ExchangePoint {

    double getCourse();

    void addTouristToQueue(Tourist tourist);

    void setListener(BiConsumer<Tourist, ExchangePoint> onServe);

    boolean isClosed();

    void open();

    void close();

}