package org.fekz115.task5.tasks12.exchange;

public class StandardExchange implements Exchange {

    private double course = 100;

    @Override
    public void changeCourse(double delta) {
        course += delta;
    }

    @Override
    public double getCourse() {
        return course;
    }

}
