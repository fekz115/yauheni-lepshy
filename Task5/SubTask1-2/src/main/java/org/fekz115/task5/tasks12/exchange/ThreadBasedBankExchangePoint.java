package org.fekz115.task5.tasks12.exchange;

import org.fekz115.task5.tasks12.presenter.Presenter;

public class ThreadBasedBankExchangePoint extends BankExchangePoint {

    public ThreadBasedBankExchangePoint(Exchange exchange, Presenter presenter, int n) {
        super(exchange, presenter, n);
    }

    @Override
    public void open() {
        Thread touristServingThread = new Thread(() -> {
            try {
                touristServingLoop();
            } catch (InterruptedException e) {
                presenter.showException(e);
            }
        });
        touristServingThread.start();
    }
}
