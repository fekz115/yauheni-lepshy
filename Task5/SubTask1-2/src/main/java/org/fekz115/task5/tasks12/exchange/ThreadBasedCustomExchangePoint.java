package org.fekz115.task5.tasks12.exchange;

import org.fekz115.task5.tasks12.presenter.Presenter;

public class ThreadBasedCustomExchangePoint extends CustomExchangePoint {

    public ThreadBasedCustomExchangePoint(Exchange exchange, Presenter presenter) {
        super(exchange, presenter);
    }

    @Override
    public void open() {
        Thread touristServingThread = new Thread(() -> {
            try {
                touristServingLoop();
            } catch (InterruptedException e) {
                presenter.showException(e);
            }
        });
        touristServingThread.start();
    }
}
