package org.fekz115.task5.tasks12.presenter;

import org.fekz115.task5.tasks12.domain.Tourist;
import org.fekz115.task5.tasks12.exchange.ExchangePoint;

public class ConsolePresenter implements Presenter {

    @Override
    public void touristArrived(Tourist tourist) {
        System.out.println("Tourist arrived: " + tourist);
    }

    @Override
    public void usedExchangePoint(ExchangePoint exchangePoint) {
        System.out.println("Used org.fekz115.task5.tasks12.exchange point: " + exchangePoint);
    }

    @Override
    public void currentCourse(double course) {
        System.out.println("Actual course: " + course);
    }

    @Override
    public void servingTourist(Tourist tourist) {
        System.out.println("Serving tourist: " + tourist);
    }

    @Override
    public void courseChanged(double oldCourse, double newCourse) {
        System.out.println("Course changed: " + oldCourse + " -> " + newCourse);
    }

    @Override
    public void showException(Throwable throwable) {
        throwable.printStackTrace(System.err);
    }

}
