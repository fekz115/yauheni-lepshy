package org.fekz115.task5.tasks12.presenter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fekz115.task5.tasks12.domain.Tourist;
import org.fekz115.task5.tasks12.exchange.ExchangePoint;

public class LoggerPresenter implements Presenter {

    private Logger logger;

    public LoggerPresenter() {
        logger = LogManager.getLogger();
    }

    public LoggerPresenter(String config){
        logger = LogManager.getLogger(config);
    }

    @Override
    public void touristArrived(Tourist tourist) {
        logger.info("Tourist arrived: " + tourist.toString());
    }

    @Override
    public void usedExchangePoint(ExchangePoint exchangePoint) {
        logger.info("Used exchange point: " + exchangePoint.toString());
    }

    @Override
    public void currentCourse(double course) {
        logger.info("Current course: " + course);
    }

    @Override
    public void servingTourist(Tourist tourist) {
        logger.info("Serving tourist: " + tourist.toString());
    }

    @Override
    public void courseChanged(double oldCourse, double newCourse) {
        logger.info("Course changed: " + oldCourse + " -> " + newCourse);
    }

    @Override
    public void showException(Throwable throwable) {
        logger.error(throwable);
    }
}
