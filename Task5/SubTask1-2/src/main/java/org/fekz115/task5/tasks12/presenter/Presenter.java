package org.fekz115.task5.tasks12.presenter;

import org.fekz115.task5.tasks12.domain.Tourist;
import org.fekz115.task5.tasks12.exchange.ExchangePoint;

public interface Presenter {

    void touristArrived(Tourist tourist);

    void usedExchangePoint(ExchangePoint exchangePoint);

    void currentCourse(double course);

    void servingTourist(Tourist tourist);

    void courseChanged(double oldCourse, double newCourse);

    void showException(Throwable throwable);

}
