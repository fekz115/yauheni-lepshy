package org.fekz115.task5.subtask4;

@FunctionalInterface
public interface Callback {

    void accept(long answer, long startTime, long endTime);

}
