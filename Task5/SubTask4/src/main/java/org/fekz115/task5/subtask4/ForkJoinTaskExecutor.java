package org.fekz115.task5.subtask4;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.LongStream;

public class ForkJoinTaskExecutor implements TaskExecutor {

    private int threadCount;

    ForkJoinTaskExecutor(int threadCount){
        this.threadCount = threadCount;
    }

    @Override
    public void calculate(long n, long startTime, Callback callback) {
        AtomicLong ans = new AtomicLong(0);
        ForkJoinPool pool = new ForkJoinPool(threadCount);
        long length = n / threadCount;
        for(int i = 0; i < threadCount; i++){
            final int temp = i;
            pool.submit(() -> {
               ans.getAndAdd(calculate(temp * length + 1, (temp+1) * length, temp));
            });
        }
        pool.shutdown();
        try {
            pool.awaitTermination(100, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        callback.accept(ans.get(), startTime, System.currentTimeMillis());
    }

    private long calculate(long start, long finish, int n){
        return LongStream.rangeClosed(start, finish).sum();
    }

    @Override
    public String toString(){
        return "ForkJoinTaskExecutor / " + threadCount;
    }
}
