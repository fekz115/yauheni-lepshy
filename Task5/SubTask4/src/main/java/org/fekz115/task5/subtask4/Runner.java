package org.fekz115.task5.subtask4;

import me.tongfei.progressbar.ProgressBar;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import static j2html.TagCreator.*;

public class Runner {

    private final static int EXPERIMENT_COUNT = 5;

    // 0 for calculate without Fork/Join Framework, 1+ for with it
    private final static int[] threads = {0, 1, 2, 4, 8, 16};

    private static long[][] resultTable = new long[EXPERIMENT_COUNT][threads.length];
    private static boolean[][] isResultCorrect = new boolean[EXPERIMENT_COUNT][threads.length];

    private final static long FINISH_NUMBER = 1_000_000_000;

    private final static long CORRECT_RESULT = LongStream.rangeClosed(1, FINISH_NUMBER).sum();

    private final static TaskExecutor[] executors = new TaskExecutor[threads.length];

    public static void main(String[] args) {
        for(int i = 0; i < executors.length; i++){
            if(threads[i] == 0){
                executors[i] = new StreamTaskExecutor();
            } else {
                executors[i] = new ForkJoinTaskExecutor(threads[i]);
            }
        }
        try (ProgressBar pb = new ProgressBar("Data capture", threads.length * EXPERIMENT_COUNT)) {
            for (int i = 0; i < resultTable.length; i++) {
                for (int j = 0; j < resultTable[i].length; j++) {
                    executors[j].calculate(FINISH_NUMBER, System.currentTimeMillis(), getCallback(i, j));
                    pb.stepBy(1);
                }
            }
        }
        try {
            FileOutputStream fos = new FileOutputStream("report.html");
            PrintStream printStream = new PrintStream(fos);
            printStream.println(createReport());
            printStream.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Callback getCallback(int i, int j) {
        return (answer, startTime, endTime) -> {
            isResultCorrect[i][j] = answer == CORRECT_RESULT;
            resultTable[i][j] = endTime - startTime;
        };
    }

    private static String[][] resultsTableContent(){
        String[][] resultsTable = new String[resultTable.length+2][threads.length+1];
        resultsTable[0][0] = "";
        for(int i = 1; i <= threads.length; i++) {
            resultsTable[0][i] = executors[i-1].toString();
        }
        for(int i = 1; i <= resultTable.length; i++) {
            resultsTable[i][0] = i + "";
        }
        resultsTable[resultsTable.length-1][0] = "Average";
        for(int i = 0; i < threads.length; i++){
            long temp = 0;
            for(int j = 0; j < resultTable.length; j++){
                temp += resultTable[j][i];
                resultsTable[j+1][i+1] = resultTable[j][i] + "";
            }
            temp /= resultTable.length;
            resultsTable[resultsTable.length-1][i+1] = temp + "";
        }
        return resultsTable;
    }

    private static String[][] resultsTableClasses(){
        String[][] correctsTable = new String[resultTable.length+2][threads.length+1];
        for (String[] strings : correctsTable) {
            Arrays.fill(strings, ".");
        }
        for(int i = 0; i < isResultCorrect.length; i++){
            for(int j = 0; j < isResultCorrect[i].length; j++){
                correctsTable[i+1][j+1] = isResultCorrect[i][j] ? ".correct" : ".incorrect";
            }
        }
        return correctsTable;
    }

    private static String createReport(){
        String[][] resultsTableContent = resultsTableContent();
        String[][] resultsTableClasses = resultsTableClasses();
        return html(
                head(
                        style("table, td {" +
                                "    border: 1px double black;" +
                                "}" +
                                "" +
                                "td {" +
                                "    padding: 0.5em;" +
                                "    text-align: center;" +
                                "}" +
                                "" +
                                ".correct {" +
                                "    color: green;" +
                                "}" +
                                "" +
                                ".incorrect {" +
                                "    color: red;" +
                                "}" +
                                "" +
                                "* {" +
                                "    font-family: Arial, serif;" +
                                "}" +
                                "" +
                                ".correct, .incorrect {" +
                                "    font-weight: bold;" +
                                "}"),
                    title("Task5: Subtask4 Report")
                ),
                body(
                    h2("Results Table"),
                    table(
                        tbody(
                            each(IntStream.rangeClosed(0, resultsTableContent.length-1).boxed().collect(Collectors.toList()), i -> tr(
                                each(IntStream.rangeClosed(0, resultsTableContent[i].length-1).boxed().collect(Collectors.toList()), j -> td(
                                    attrs(resultsTableClasses[i][j]), resultsTableContent[i][j]
                                ))
                            ))
                        )
                    ),
                    hr(),
                    h2("Hardware Details"),
                    ul(
                        li("Processor: " + System.getenv("PROCESSOR_IDENTIFIER")),
                        li("Arch: " + System.getenv("PROCESSOR_ARCHITECTURE")),
                        li("Number of processors(cores): " + Runtime.getRuntime().availableProcessors()),
                        li("OS: " + System.getProperty("os.name") + " " + System.getProperty("os.arch") + " " + System.getProperty("os.version"))
                    )
                )
        ).render();
    }

}
