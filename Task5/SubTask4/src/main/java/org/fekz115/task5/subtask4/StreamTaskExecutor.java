package org.fekz115.task5.subtask4;

import java.util.stream.LongStream;

public class StreamTaskExecutor implements TaskExecutor {
    @Override
    public void calculate(long n, long startTime, Callback callback) {
        long ans = LongStream.rangeClosed(1, n).sum();
        callback.accept(ans, startTime, System.currentTimeMillis());
    }

    @Override
    public String toString(){
        return "StreamTaskExecutor";
    }
}
