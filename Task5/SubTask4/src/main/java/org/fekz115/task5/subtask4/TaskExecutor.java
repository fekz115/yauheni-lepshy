package org.fekz115.task5.subtask4;

public interface TaskExecutor {

    void calculate(long n, long startTime, Callback callback);

}
