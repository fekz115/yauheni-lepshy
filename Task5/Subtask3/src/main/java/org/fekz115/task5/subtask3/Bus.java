package org.fekz115.task5.subtask3;

import org.fekz115.task5.subtask3.domain.Tourist;
import org.fekz115.task5.subtask3.waypoint.Waypoint;

import java.util.concurrent.Phaser;

public class Bus {

    private Waypoint[] route;
    private Presenter presenter;

    public Bus(Waypoint[] route, Presenter presenter) {
        this.route = route;
        this.presenter = presenter;
    }

    public void startTour(Tourist[] tourists){
        presenter.onRouteStart();
        Phaser phaser = new Phaser();
        for (Tourist tourist : tourists) {
            tourist.setPhaser(phaser);
            tourist.setInTravel(true);
        }
        for(Waypoint waypoint : route) {
            presenter.onWaypoint(waypoint);
            waypoint.serveTourists(tourists, presenter);
            phaser.awaitAdvance(phaser.getPhase());
        }
        for (Tourist tourist : tourists) {
            tourist.setPhaser(null);
            tourist.setInTravel(false);
        }
        presenter.onRouteEnd();
        presenter.logInfo("Photos in cloud count: " + Cloud.getCloud().getPhotos());
    }

}
