package org.fekz115.task5.subtask3;

import java.util.concurrent.atomic.AtomicInteger;

public class Cloud {

    private static Cloud cloud = null;

    private Cloud(){};

    private AtomicInteger photos = new AtomicInteger(0);

    public static synchronized Cloud getCloud(){
        if(cloud == null) {
            cloud = new Cloud();
        }
        return cloud;
    }

    public int loadPhotos(int photosCount){
        photos.getAndAdd(photosCount);
        return 0;
    }

    public int getPhotos() {
        return photos.get();
    }

}
