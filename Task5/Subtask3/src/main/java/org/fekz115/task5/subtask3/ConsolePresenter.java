package org.fekz115.task5.subtask3;

import org.fekz115.task5.subtask3.domain.Tourist;
import org.fekz115.task5.subtask3.waypoint.Waypoint;

public class ConsolePresenter implements Presenter {
    @Override
    public void onRouteStart() {
        System.out.println("Rout started");
    }

    @Override
    public void onRouteEnd() {
        System.out.println("Rout ended");
    }

    @Override
    public void onTourist(Tourist tourist) {
        System.out.println(tourist);
    }

    @Override
    public void onWaypoint(Waypoint waypoint) {
        System.out.println("Waypoint reached: " + waypoint);
    }

    @Override
    public void logInfo(String info) {
        System.out.print(info);
    }

    @Override
    public void logError(Throwable t) {
        t.printStackTrace();
    }


}
