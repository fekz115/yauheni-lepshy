package org.fekz115.task5.subtask3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fekz115.task5.subtask3.domain.Tourist;
import org.fekz115.task5.subtask3.waypoint.Waypoint;

public class LoggerPresenter implements Presenter {

    private static Logger logger = LogManager.getLogger(Runner.class);

    @Override
    public void onRouteStart() {
        logger.info("Rout started");
    }

    @Override
    public void onRouteEnd() {
        logger.info("Rout ended");
    }

    @Override
    public void onTourist(Tourist tourist) {
        logger.info(tourist.toString());
    }

    @Override
    public void onWaypoint(Waypoint waypoint) {
        logger.info("Waypoint reached: " + waypoint.toString());
    }

    @Override
    public void logInfo(String info) {
        logger.info(info);
    }

    @Override
    public void logError(Throwable t) {
        logger.error(t);
    }
}
