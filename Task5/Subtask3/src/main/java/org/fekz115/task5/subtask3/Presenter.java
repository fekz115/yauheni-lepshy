package org.fekz115.task5.subtask3;

import org.fekz115.task5.subtask3.domain.Tourist;
import org.fekz115.task5.subtask3.waypoint.Waypoint;

public interface Presenter {

    void onRouteStart();

    void onRouteEnd();

    void onTourist(Tourist tourist);

    void onWaypoint(Waypoint waypoint);

    void logInfo(String info);

    void logError(Throwable t);

}
