package org.fekz115.task5.subtask3;

import org.fekz115.task5.subtask3.domain.Tourist;
import java.util.function.Consumer;

public class RunnableSupplier {

    public Runnable apply(Tourist tourist, Presenter presenter, Consumer<Tourist> onServeEnd) {
        return () -> {
            try {
                Thread.sleep(tourist.getDelay());
            } catch (InterruptedException e) {
                presenter.logError(e);
            }
            tourist.makePhoto();
            if(onServeEnd != null) {
                onServeEnd.accept(tourist);
            }
            tourist.getPhaser().arrive();
            tourist.setPhotoCount(Cloud.getCloud().loadPhotos(tourist.getPhotoCount()));
        };
    }

}
