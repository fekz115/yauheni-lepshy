package org.fekz115.task5.subtask3;

import org.fekz115.task5.subtask3.domain.Tourist;
import org.fekz115.task5.subtask3.waypoint.*;

public class Runner {

    private static Waypoint[] route = new Waypoint[]{
            new Praha(),
            new Paris(),
            new Rome(),
            new Spain(),
    };
    private static Presenter presenter = new LoggerPresenter();

    private final static int GROUPS_COUNT = 2;
    private final static int GROUP_CAPACITY = 40;

    public static void main(String[] args) {
        Bus bus = new Bus(route, presenter);
        TouristSupplier supplier = new TouristSupplier();
        for(int j = 0; j < GROUPS_COUNT; j++) {
            Tourist[] tourists = new Tourist[GROUP_CAPACITY];
            for (int i = 0; i < tourists.length; i++) {
                tourists[i] = supplier.get();
            }
            bus.startTour(tourists);
        }
    }

}
