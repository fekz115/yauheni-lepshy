package org.fekz115.task5.subtask3;

import org.fekz115.task5.subtask3.domain.Tourist;

import java.util.function.Supplier;

public class TouristSupplier implements Supplier<Tourist> {

    private final static int MAX_DELAY = 5000;

    private static int number = 0;

    private static Supplier<Long> randomizer = () -> (long)(Math.random()*MAX_DELAY);

    @Override
    public Tourist get() {
        return new Tourist("Tourist " + number++, randomizer.get());
    }
}
