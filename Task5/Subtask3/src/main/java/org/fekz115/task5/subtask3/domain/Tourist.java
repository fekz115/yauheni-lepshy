package org.fekz115.task5.subtask3.domain;

import java.util.concurrent.Phaser;

public class Tourist {

    private String name;
    private boolean isInTravel = false;
    private long delay;

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    private int photoCount = 0;

    public Phaser getPhaser() {
        return phaser;
    }

    public void setPhaser(Phaser phaser) {
        if(this.phaser != null) {
            this.phaser.arriveAndDeregister();
        }
        this.phaser = phaser;
        if(phaser != null) {
            phaser.register();
        }
    }

    private Phaser phaser = null;

    public int getPhotoCount() {
        return photoCount;
    }

    public boolean isInTravel() {
        return isInTravel;
    }

    public void setInTravel(boolean inTravel) {
        isInTravel = inTravel;
    }

    public long getDelay() {
        return delay;
    }

    public void makePhoto(){
        photoCount++;
    }



    public Tourist(String name, long delay) {
        this.name = name;
        this.delay = delay;
    }

    @Override
    public String toString() {
        return "Tourist{" +
                "name='" + name + '\'' +
                ", isInTravel=" + isInTravel +
                ", photoCount=" + photoCount +
                '}';
    }
}
