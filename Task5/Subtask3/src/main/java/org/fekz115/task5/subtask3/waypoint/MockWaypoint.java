package org.fekz115.task5.subtask3.waypoint;

import org.fekz115.task5.subtask3.Presenter;
import org.fekz115.task5.subtask3.RunnableSupplier;
import org.fekz115.task5.subtask3.domain.Tourist;

// Only for testing
public class MockWaypoint implements Waypoint {

    @Override
    public void serveTourists(Tourist[] tourists, Presenter presenter) {
        RunnableSupplier runnableSupplier = new RunnableSupplier();
        for (Tourist tourist : tourists) {
            presenter.onTourist(tourist);
            Thread thread = new Thread(runnableSupplier.apply(tourist, presenter, null));
            thread.start();
        }
    }
}
