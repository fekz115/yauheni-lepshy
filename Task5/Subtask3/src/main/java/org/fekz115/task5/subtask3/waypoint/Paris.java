package org.fekz115.task5.subtask3.waypoint;

import org.fekz115.task5.subtask3.Presenter;
import org.fekz115.task5.subtask3.RunnableSupplier;
import org.fekz115.task5.subtask3.domain.Tourist;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Paris implements Waypoint {

    private final static int TOURISTS_LOOKING_TO_PICTURE_COUNT = 5;

    @Override
    public void serveTourists(Tourist[] tourists, Presenter presenter) {
        ExecutorService service = Executors.newFixedThreadPool(TOURISTS_LOOKING_TO_PICTURE_COUNT);
        RunnableSupplier runnableSupplier = new RunnableSupplier();
        for (Tourist tourist : tourists) {
            service.submit(runnableSupplier.apply(tourist, presenter, (t) -> {
                presenter.logInfo("Tourist finished looking at picture: ");
                presenter.onTourist(t);
            }));
        }
        service.shutdown();
    }

}
