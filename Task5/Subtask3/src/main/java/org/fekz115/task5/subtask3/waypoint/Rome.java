package org.fekz115.task5.subtask3.waypoint;

import org.fekz115.task5.subtask3.Presenter;
import org.fekz115.task5.subtask3.RunnableSupplier;
import org.fekz115.task5.subtask3.domain.Tourist;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.*;

public class Rome implements Waypoint {

    private final static int QUEUE_LENGTH = 5;

    @Override
    public void serveTourists(Tourist[] tourists, Presenter presenter) {
        RunnableSupplier runnableSupplier = new RunnableSupplier();
        CyclicBarrier pizzaBarrier = new CyclicBarrier(QUEUE_LENGTH);
        CyclicBarrier pastaBarrier = new CyclicBarrier(QUEUE_LENGTH);
        Queue<Tourist> pizzaQueue = new LinkedList<>();
        Queue<Tourist> pastaQueue = new LinkedList<>();
        for (int i = 0; i < tourists.length; i++) {
            presenter.onTourist(tourists[i]);
            if(i % 2 == 0) {
                presenter.logInfo("Tourist added to pizza queue: ");
                pizzaQueue.add(tourists[i]);
            } else {
                presenter.logInfo("Tourist added to pasta queue: ");
                pastaQueue.add(tourists[i]);
            }
        }
        ExecutorService service = Executors.newFixedThreadPool(2);
        ExecutorService pizzaService = Executors.newFixedThreadPool(QUEUE_LENGTH);
        ExecutorService pastaService = Executors.newFixedThreadPool(QUEUE_LENGTH);
        processQueue(presenter, runnableSupplier, pizzaBarrier, pizzaQueue, pizzaService);
        processQueue(presenter, runnableSupplier, pastaBarrier, pastaQueue, pastaService);
        service.shutdown();
        pizzaService.shutdown();
        pastaService.shutdown();
    }

    private void processQueue(Presenter presenter, RunnableSupplier runnableSupplier, CyclicBarrier barrier, Queue<Tourist> queue, ExecutorService service) {
        while(!queue.isEmpty()){
            Runnable runnable = runnableSupplier.apply(queue.poll(), presenter, (tourist -> {
                presenter.logInfo("Tourist finish eating: ");
                presenter.onTourist(tourist);
                try {
                    barrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }));
            service.submit(runnable);
        }
    }

}
