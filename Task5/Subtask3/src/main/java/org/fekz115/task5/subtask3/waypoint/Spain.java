package org.fekz115.task5.subtask3.waypoint;

import org.fekz115.task5.subtask3.Presenter;
import org.fekz115.task5.subtask3.RunnableSupplier;
import org.fekz115.task5.subtask3.domain.Tourist;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Spain implements Waypoint {
    @Override
    public void serveTourists(Tourist[] tourists, Presenter presenter) {
        ExecutorService service = Executors.newCachedThreadPool();
        RunnableSupplier runnableSupplier = new RunnableSupplier();
        for (Tourist t : tourists) {
            service.submit(runnableSupplier.apply(t, presenter, tourist -> {
                presenter.logInfo("Tourist finished relaxing: ");
                presenter.onTourist(tourist);
            }));
        }
    }
}
