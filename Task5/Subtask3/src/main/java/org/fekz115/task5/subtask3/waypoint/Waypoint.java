package org.fekz115.task5.subtask3.waypoint;

import org.fekz115.task5.subtask3.Presenter;
import org.fekz115.task5.subtask3.domain.Tourist;

public interface Waypoint {

    void serveTourists(Tourist[] tourists, Presenter presenter);

}
