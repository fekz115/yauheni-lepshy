Зайти на https://www.w3schools.com/sql/trysql.asp?filename=trysql_op_in	

Приложить запросы которые соответствуют результатам ниже


1.	Найти товары с максимальной ценой. 
`SELECT *, MAX(Price) FROM Products;`


| ProductID | ProductName   | SupplierID | CategoryID | Unit               | Price  |
|----------:|:-------------:|:----------:|:----------:|:------------------:|:-------|
|        38 | Cute de Blaye |         18 |          1 | 12 - 75 cl bottles | 263.50 |

2.	Выбрать все товары, у которых Unit '10 pkgs.'.
`SELECT * FROM Products WHERE Unit='10 pkgs.';`

| ProductID | ProductName | SupplierID | CategoryID | Unit     | Price |
|----------:|:-----------:|:----------:|:----------:|:--------:|:------|
|        48 | Chocolade   |         22 |          3 | 10 pkgs. | 12.75 |

3.	Выбрать адреса поставщиков, которые проживают в одном из городов: Tokyo, Frankfurt, Osaka. 
`SELECT Address FROM Suppliers WHERE City IN ('Tokyo', 'Frankfurt', 'Osaka');`

| Address                   |
|---------------------------|
| 9-8 Sekimai Musashino-shi |
| 92 Setsuko Chuo-ku        |
| Bogenallee 51             |

4.	 Выбрать названия товаров, у которых Price = 13 или 15 и отсортировать по возрастанию, использовать Select команды с объединением результатов через UNION.
`SELECT ProductName FROM Products WHERE Price in ('13', '15');`
`SELECT ProductName FROM (SELECT * FROM Products WHERE Price=13 UNION SELECT * FROM Products WHERE Price=15 ORDER BY Price DESC);`

| ProductName                     |
|---------------------------------|
| Original Frankfurter grine Soae |
| Outback Lager                   |
| Rud Kaviar                      |

5.	Выбрать все товары, у которых поставщик «Grandma Kelly's Homestead» и цена > 27. В результате вывести 3 колонки: Product, Supplier, Price.
`SELECT ProductName, SupplierName, Price FROM Products INNER JOIN Suppliers ON Products.SupplierID == Suppliers.SupplierID WHERE SupplierName == "Grandma Kelly's Homestead" AND Price > 27;`

| ProductName                     | SupplierName              | Price |
|--------------------------------:|:-------------------------:|:------|
| Uncle Bob's Organic Dried Pears | Grandma Kelly's Homestead | 30.00 |
| Northwoods Cranberry Sauce      | Grandma Kelly's Homestead | 40.00 |

6.	Показать названия продуктов и их категорий, которые используются в заказах от заказчика по имени Blondel père et fils и категории которых состоят как минимум из 2-х слов.
`SELECT CategoryName, ProductName FROM Customers INNER JOIN Orders ON Customers.CustomerID == Orders.CustomerID INNER JOIN OrderDetails ON Orders.OrderID == OrderDetails.OrderID INNER JOIN Products ON Products.ProductID == OrderDetails.ProductID INNER JOIN Categories ON Categories.CategoryID == Products.CategoryID WHERE CustomerName == "Blondel père et fils" AND CategoryName LIKE '% %'; `

| CategoryName   | ProductName            |
|---------------:|:-----------------------|
| Dairy Products | Mozzarella di Giovanni |

7.	 *Найти среднюю стоимость приправ (Condiments) отправленных в штаты, заказы на которые оформлены Margaret Peacock, вывести стоимость округленную до 2-х знаков после запятой (колонку назвать Average)
`SELECT ROUND(AVG(Price), 2) FROM Orders INNER JOIN Customers ON Customers.CustomerID == Orders.CustomerID INNER JOIN Employees ON Orders.EmployeeID == Employees.EmployeeID INNER JOIN OrderDetails ON Orders.OrderID == OrderDetails.OrderID INNER JOIN Products ON OrderDetails.ProductID == Products.ProductID INNER JOIN Categories ON Products.CategoryID == Categories.CategoryID WHERE Customers.Country == 'USA' AND CategoryName == 'Condiments' AND Employees.FirstName == 'Margaret' AND Employees.LastName == 'Peacock';`

| Average |
|---------|
|   30.17 |

8.	 Найти количество клиентов, которые НЕ проживают в Франции и Германии,  столбец назвать Countt.
`SELECT COUNT() AS 'Countt' FROM Customers WHERE Country NOT IN ('France', 'Germany');`

| Countt |
|--------|
|     69 |

9.	 Вывести среднюю сумму товаров, поставляемых в бутылках, округлив до 2-х знаков после запятой, столбец назвать Summ. 
`SELECT ROUND(AVG(Price), 2) AS 'Summ' FROM [Products] WHERE Unit LIKE '%bottles%';`

| Summ  |
|-------|
| 37.03 |


10.	 Вывести сумму всех товаров, в названии которых содержится "od”, столбец назвать Summ.
`SELECT SUM(Price) AS 'Summ' FROM [Products] WHERE ProductName LIKE '%od%';`

| Summ   |
|--------|
| 131.00 |
