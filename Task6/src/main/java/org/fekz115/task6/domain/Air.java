package org.fekz115.task6.domain;

import javax.xml.bind.annotation.*;
import java.time.Duration;
import java.util.List;

@XmlType(propOrder = {"direction", "totalTravelTime", "options", "extension"})
public class Air {

    private String Direction;

    private Duration TotalTravelTime;

    private List<Option> options;

    private Extension extension;

    @XmlType(name="AirExtension")
    public static class Extension {
        @XmlElement(name = "Attributes")
        Attributes attributes;
    }

    @XmlAttribute(name = "Direction")
    public String getDirection() {
        return Direction;
    }

    public void setDirection(String direction) {
        Direction = direction;
    }

    @XmlAttribute(name = "TotalTravelTime")
    public String getTotalTravelTime() {
        return TotalTravelTime.toString();
    }

    public void setTotalTravelTime(String totalTravelTime) {
        TotalTravelTime = Duration.parse(totalTravelTime);
    }

    @XmlElementWrapper(name = "Options")
    @XmlElement(name = "Option")
    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    @XmlElement(name = "Extension")
    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }
}
