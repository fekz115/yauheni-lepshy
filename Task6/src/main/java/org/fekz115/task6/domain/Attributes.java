package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;

public class Attributes {

    private String tripType;

    @XmlAttribute(name = "TripType")
    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }
}
