package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;

public class BookingClassAvailability {

    private String BookCode;
    private String BookDesig;
    private String Cabin;

    @XmlAttribute(name = "BookCode")
    public String getBookCode() {
        return BookCode;
    }

    public void setBookCode(String bookCode) {
        BookCode = bookCode;
    }

    @XmlAttribute(name = "BookDesig")
    public String getBookDesig() {
        return BookDesig;
    }

    public void setBookDesig(String bookDesig) {
        BookDesig = bookDesig;
    }

    @XmlAttribute(name = "Cabin")
    public String getCabin() {
        return Cabin;
    }

    public void setCabin(String cabin) {
        Cabin = cabin;
    }
}
