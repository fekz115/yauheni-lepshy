package org.fekz115.task6.domain;

import util.Utils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.time.LocalDateTime;
import java.util.List;

@XmlType(propOrder = {"departureDate", "fareReference", "extension"})
public class FareInfo {

    private LocalDateTime departureDate;
    private String fareReference;

    private Extension extension;

    @XmlType(name="FareInfoExtension",
    propOrder = {"base", "taxInfos", "feeInfos", "total", "segments", "passenger", "carrier"})
    public static class Extension {
        private Sum base;
        private List<TaxInfo> taxInfos;
        private List<FeeInfo> feeInfos;
        private Sum total;
        private List<Segment> segments;
        private Passenger passenger;
        private String carrier;

        @XmlElement(name = "Base")
        public Sum getBase() {
            return base;
        }

        public void setBase(Sum base) {
            this.base = base;
        }

        @XmlElementWrapper(name = "TaxInfos")
        @XmlElement(name = "TaxInfo")
        public List<TaxInfo> getTaxInfos() {
            return taxInfos;
        }

        public void setTaxInfos(List<TaxInfo> taxInfos) {
            this.taxInfos = taxInfos;
        }

        @XmlElementWrapper(name = "FeeInfos")
        @XmlElement(name = "FeeInfo")
        public List<FeeInfo> getFeeInfos() {
            return feeInfos;
        }

        public void setFeeInfos(List<FeeInfo> feeInfos) {
            this.feeInfos = feeInfos;
        }

        @XmlElement(name = "Total")
        public Sum getTotal() {
            return total;
        }

        public void setTotal(Sum total) {
            this.total = total;
        }

        @XmlElement(name = "Segment")
        public List<Segment> getSegments() {
            return segments;
        }

        public void setSegments(List<Segment> segments) {
            this.segments = segments;
        }

        @XmlElement(name = "Passenger")
        public Passenger getPassenger() {
            return passenger;
        }

        public void setPassenger(Passenger passenger) {
            this.passenger = passenger;
        }

        @XmlElement(name = "Carrier")
        public String getCarrier() {
            return carrier;
        }

        public void setCarrier(String carrier) {
            this.carrier = carrier;
        }
    }

    @XmlElement(name = "DepartureDate")
    public String getDepartureDate() {
        return Utils.dateToString(departureDate);
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = Utils.stringToDate(departureDate);
    }

    @XmlElement(name = "FareReference")
    public String getFareReference() {
        return fareReference;
    }

    public void setFareReference(String fareReference) {
        this.fareReference = fareReference;
    }

    @XmlElement(name = "Extension")
    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }
}
