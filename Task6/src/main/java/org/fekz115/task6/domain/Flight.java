package org.fekz115.task6.domain;

import util.Utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.time.LocalDateTime;

@XmlType(propOrder = {"departureDateTime", "arrivalDateTime", "bookCode", "stopQuantity", "RPH", "flightNumber", "numberInParty",
                        "extension"})
public class Flight {

    private LocalDateTime departureDateTime;
    private LocalDateTime arrivalDateTime;
    private String bookCode;
    private int stopQuantity;
    private int RPH;
    private String flightNumber;
    private int numberInParty;

    private Extension extension;

    @XmlType(name="FlightExtension",
            propOrder = {"flightSupplementalInfo", "bookingClassAvailability", "eticket", "terminalInformation", "inventorySystem"})
    public static class Extension {
        private FlightSupplementalInfo flightSupplementalInfo;
        private BookingClassAvailability bookingClassAvailability;
        private String eticket;
        private TerminalInformation terminalInformation;
        private InventorySystem inventorySystem;

        @XmlElement(name = "FlightSupplementalInfo")
        public FlightSupplementalInfo getFlightSupplementalInfo() {
            return flightSupplementalInfo;
        }

        public void setFlightSupplementalInfo(FlightSupplementalInfo flightSupplementalInfo) {
            this.flightSupplementalInfo = flightSupplementalInfo;
        }

        @XmlElement(name = "BookingClassAvailability")
        public BookingClassAvailability getBookingClassAvailability() {
            return bookingClassAvailability;
        }

        public void setBookingClassAvailability(BookingClassAvailability bookingClassAvailability) {
            this.bookingClassAvailability = bookingClassAvailability;
        }

        @XmlElement(name = "Eticket")
        public String getEticket() {
            return eticket;
        }

        public void setEticket(String eticket) {
            this.eticket = eticket;
        }

        @XmlElement(name = "TerminalInformation")
        public TerminalInformation getTerminalInformation() {
            return terminalInformation;
        }

        public void setTerminalInformation(TerminalInformation terminalInformation) {
            this.terminalInformation = terminalInformation;
        }

        @XmlElement(name = "InventorySystem")
        public InventorySystem getInventorySystem() {
            return inventorySystem;
        }

        public void setInventorySystem(InventorySystem inventorySystem) {
            this.inventorySystem = inventorySystem;
        }
    }

    @XmlAttribute(name = "DepartureDateTime")
    public String getDepartureDateTime() {
        return Utils.dateToString(departureDateTime);
    }

    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = Utils.stringToDate(departureDateTime);
    }

    @XmlAttribute(name = "ArrivalDateTime")
    public String getArrivalDateTime() {
        return Utils.dateToString(arrivalDateTime);
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = Utils.stringToDate(arrivalDateTime);
    }

    @XmlAttribute(name = "BookCode")
    public String getBookCode() {
        return bookCode;
    }

    public void setBookCode(String bookCode) {
        this.bookCode = bookCode;
    }

    @XmlAttribute(name = "StopQuantity")
    public int getStopQuantity() {
        return stopQuantity;
    }

    public void setStopQuantity(int stopQuantity) {
        this.stopQuantity = stopQuantity;
    }

    @XmlAttribute(name = "RPH")
    public int getRPH() {
        return RPH;
    }

    public void setRPH(int RPH) {
        this.RPH = RPH;
    }

    @XmlAttribute(name = "FlightNumber")
    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @XmlAttribute(name = "NumberInParty")
    public int getNumberInParty() {
        return numberInParty;
    }

    public void setNumberInParty(int numberInParty) {
        this.numberInParty = numberInParty;
    }

    @XmlElement(name = "Extension")
    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }
}
