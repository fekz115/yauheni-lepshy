package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;
import java.time.Duration;

public class FlightSupplementalInfo {

    private String FlyingTime;

    @XmlAttribute(name = "FlyingTime")
    public String getFlyingTime() {
        return FlyingTime.toString();
    }

    public void setFlyingTime(String flyingTime) {
        FlyingTime = flyingTime;
    }
}
