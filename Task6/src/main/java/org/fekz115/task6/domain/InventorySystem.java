package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;

public class InventorySystem {

    private String name;

    @XmlAttribute(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
