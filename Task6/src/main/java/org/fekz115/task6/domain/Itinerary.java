package org.fekz115.task6.domain;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Itinerary")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {"sequenceNumber", "RPH", "itineraryID", "air", "pricingInfo", "ticketingInfo"})
public class Itinerary {

    private int SequenceNumber;

    private int RPH;

    private int ItineraryID;

    private Air air;
    private PricingInfo pricingInfo;
    private TicketingInfo TicketingInfo;

    @XmlAttribute(name = "SequenceNumber")
    public int getSequenceNumber() {
        return SequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        SequenceNumber = sequenceNumber;
    }

    @XmlAttribute(name = "RPH")
    public int getRPH() {
        return RPH;
    }

    public void setRPH(int RPH) {
        this.RPH = RPH;
    }

    @XmlAttribute(name = "ItineraryID")
    public int getItineraryID() {
        return ItineraryID;
    }

    public void setItineraryID(int itineraryID) {
        ItineraryID = itineraryID;
    }

    @XmlElement(name = "Air")
    public Air getAir() {
        return air;
    }

    public void setAir(Air air) {
        this.air = air;
    }

    @XmlElement(name = "PricingInfo")
    public PricingInfo getPricingInfo() {
        return pricingInfo;
    }

    public void setPricingInfo(PricingInfo pricingInfo) {
        this.pricingInfo = pricingInfo;
    }


    @XmlElement(name = "TicketingInfo")
    public TicketingInfo getTicketingInfo() {
        return TicketingInfo;
    }

    public void setTicketingInfo(org.fekz115.task6.domain.TicketingInfo ticketingInfo) {
        TicketingInfo = ticketingInfo;
    }
}
