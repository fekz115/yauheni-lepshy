package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder = {"flights", "extension"})
public class Option {

    private List<Flight> flights;
    private Extension extension;

    @XmlElement(name = "Flight")
    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    @XmlElement(name = "Extension")
    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }

    @XmlType(name="OptionExtension")
    public static class Extension {
        @XmlElement(name = "InventorySystem")
        public InventorySystem getInventorySystem() {
            return inventorySystem;
        }

        public void setInventorySystem(InventorySystem inventorySystem) {
            this.inventorySystem = inventorySystem;
        }

        @XmlElement(name = "Source")
        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        private InventorySystem inventorySystem;
        private String source;
    }

}
