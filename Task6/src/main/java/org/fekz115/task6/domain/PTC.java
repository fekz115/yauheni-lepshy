package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder = {"source", "passengerQuantity", "basisCodes", "passengerFare", "extension"})
public class PTC {

    private String source;
    private List<String> basisCodes;
    private PassengerFare passengerFare;
    private Extension extension;
    private PassengerQuantity passengerQuantity;

    @XmlElement(name = "PassengerQuantity")
    public PassengerQuantity getPassengerQuantity() {
        return passengerQuantity;
    }

    public void setPassengerQuantity(PassengerQuantity passengerQuantity) {
        this.passengerQuantity = passengerQuantity;
    }

    @XmlAttribute(name = "Source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @XmlElementWrapper(name = "BasisCodes")
    @XmlElement(name = "BasisCode")
    public List<String> getBasisCodes() {
        return basisCodes;
    }

    public void setBasisCodes(List<String> basisCodes) {
        this.basisCodes = basisCodes;
    }

    @XmlElement(name = "PassengerFare")
    public PassengerFare getPassengerFare() {
        return passengerFare;
    }

    public void setPassengerFare(PassengerFare passengerFare) {
        this.passengerFare = passengerFare;
    }

    @XmlElement(name = "Extension")
    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }
    @XmlType(name="PTCExtension")
    public static class Extension {
        private Passenger passenger;

        @XmlElement(name = "Passenger")
        public Passenger getPassenger() {
            return passenger;
        }

        public void setPassenger(Passenger passenger) {
            this.passenger = passenger;
        }
    }

}
