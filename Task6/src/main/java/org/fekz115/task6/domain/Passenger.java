package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"code", "reference", "age", "remoteSystemPTCs"})
public class Passenger {

    private String code;
    private int reference;
    private int age;

    private RemoteSystemPTCs remoteSystemPTCs;

    @XmlAttribute(name = "PassengerCode")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlAttribute(name = "PassengerReference")
    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    @XmlAttribute(name = "PassengerAge")
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @XmlElement(name = "RemoteSystemPTCs")
    public RemoteSystemPTCs getRemoteSystemPTCs() {
        return remoteSystemPTCs;
    }

    public void setRemoteSystemPTCs(RemoteSystemPTCs remoteSystemPTCs) {
        this.remoteSystemPTCs = remoteSystemPTCs;
    }
}
