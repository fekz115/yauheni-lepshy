package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder = {"base", "taxes", "fees", "total", "extension"})
public class PassengerFare {

    private Sum base;

    private List<Tax> taxes;
    private List<Fee> fees;

    private Sum total;


    private Extension extension;

    @XmlType(name="PassengerFareExtension", propOrder = {"taxInfos", "feeInfos"})
    public static class Extension {
        TaxInfo[] taxInfos;
        FeeInfo[] feeInfos;

        @XmlElementWrapper(name = "TaxInfos")
        @XmlElement(name = "TaxInfo")
        public TaxInfo[] getTaxInfos() {
            return taxInfos;
        }

        public void setTaxInfos(TaxInfo[] taxInfos) {
            this.taxInfos = taxInfos;
        }

        @XmlElementWrapper(name = "FeeInfos")
        @XmlElement(name = "FeeInfo")
        public FeeInfo[] getFeeInfos() {
            return feeInfos;
        }

        public void setFeeInfos(FeeInfo[] feeInfos) {
            this.feeInfos = feeInfos;
        }
    }

    @XmlElement(name = "Base")
    public Sum getBase() {
        return base;
    }

    public void setBase(Sum base) {
        this.base = base;
    }

    @XmlElementWrapper(name = "Taxes")
    @XmlElement(name = "Tax")
    public List<Tax> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Tax> taxes) {
        this.taxes = taxes;
    }

    @XmlElementWrapper(name = "Fees")
    @XmlElement(name = "Fee")
    public List<Fee> getFees() {
        return fees;
    }

    public void setFees(List<Fee> fees) {
        this.fees = fees;
    }

    @XmlElement(name = "Total")
    public Sum getTotal() {
        return total;
    }

    public void setTotal(Sum total) {
        this.total = total;
    }

    @XmlElement(name = "Extension")
    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }
}
