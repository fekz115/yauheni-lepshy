package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder = {"total", "PTCs", "fareInfos", "extension"})
public class PricingInfo {

    @XmlAttribute(name = "Source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @XmlElement(name = "Total")
    public PassengerFare getTotal() {
        return total;
    }

    public void setTotal(PassengerFare total) {
        this.total = total;
    }

    @XmlElementWrapper(name = "PTCs")
    @XmlElement(name = "PTC")
    public List<PTC> getPTCs() {
        return PTCs;
    }

    public void setPTCs(List<PTC> PTCs) {
        this.PTCs = PTCs;
    }

    @XmlElementWrapper(name = "FareInfos")
    @XmlElement(name = "FareInfo")
    public List<FareInfo> getFareInfos() {
        return fareInfos;
    }

    public void setFareInfos(List<FareInfo> fareInfos) {
        this.fareInfos = fareInfos;
    }

    @XmlElement(name = "Extension")
    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }

    private String source;
    private PassengerFare total;
    private List<PTC> PTCs;
    private List<FareInfo> fareInfos;
    private Extension extension;

    @XmlType(name="PricingInfoExtension")
    public static class Extension {
        private String pricingSystem;
        private Provider provider;

        @XmlElement(name = "PricingSystem")
        public String getPricingSystem() {
            return pricingSystem;
        }

        public void setPricingSystem(String pricingSystem) {
            this.pricingSystem = pricingSystem;
        }

        @XmlElement(name = "Provider")
        public Provider getProvider() {
            return provider;
        }

        public void setProvider(Provider provider) {
            this.provider = provider;
        }
    }
}
