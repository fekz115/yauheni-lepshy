package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;

public class Provider {

    private String providerCode;

    @XmlAttribute(name = "ProviderCode")
    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }
}
