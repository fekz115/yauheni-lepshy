package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;

public class RemoteSystemPTCs {

    private String passengerCode;

    @XmlAttribute(name = "PassengerCode")
    public String getPassengerCode() {
        return passengerCode;
    }

    public void setPassengerCode(String passengerCode) {
        this.passengerCode = passengerCode;
    }
}
