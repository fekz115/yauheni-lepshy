package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;

public class Segment {

    private int FlightReference;

    @XmlAttribute(name = "FlightReference")
    public int getFlightReference() {
        return FlightReference;
    }

    public void setFlightReference(int flightReference) {
        FlightReference = flightReference;
    }
}
