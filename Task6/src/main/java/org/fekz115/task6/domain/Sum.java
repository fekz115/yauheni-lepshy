package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;

public class Sum {

    private double amount;
    private String currencyCode;

    @XmlAttribute(name = "Amount")
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @XmlAttribute(name = "CurrencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
