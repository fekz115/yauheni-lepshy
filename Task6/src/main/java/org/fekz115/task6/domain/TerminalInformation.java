package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlElement;

public class TerminalInformation {

    private int[] terminal;

    @XmlElement(name = "terminal")
    public int[] getTerminal() {
        return terminal;
    }

    public void setTerminal(int[] terminal) {
        this.terminal = terminal;
    }
}
