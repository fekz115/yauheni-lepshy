package org.fekz115.task6.domain;

import javax.xml.bind.annotation.XmlAttribute;

public class TicketingInfo {

    private String TicketType;

    @XmlAttribute(name = "TicketType")
    public String getTicketType() {
        return TicketType;
    }

    public void setTicketType(String ticketType) {
        TicketType = ticketType;
    }
}
