package util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {

    private final static DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

    public static String dateToString(LocalDateTime date) {
        return formatter.format(date);
    }

    public static LocalDateTime stringToDate(String string) {
        return LocalDateTime.parse(string);
    }

}
