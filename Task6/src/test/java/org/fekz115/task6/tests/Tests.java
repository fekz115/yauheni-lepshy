package org.fekz115.task6.tests;

import org.fekz115.task6.domain.Itinerary;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class Tests {

    @Test
    public void unmarshallTest() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Itinerary.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Itinerary itinerary = (Itinerary) unmarshaller.unmarshal(Tests.class.getResourceAsStream("/testItinerary.xml"));
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(itinerary, new File("outputItinerary.xml"));
    }

}
