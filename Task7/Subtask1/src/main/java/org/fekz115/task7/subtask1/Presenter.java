package org.fekz115.task7.subtask1;

import org.fekz115.task7.subtask1.domain.Profile;

import java.util.List;

@FunctionalInterface
public interface Presenter {

    void logProfile(Profile profile);

    static void printProfile(Profile profile) {
        System.out.println(profile);
    }

    default void logAllProfiles(List<Profile> profiles) {
        profiles.forEach(this::logProfile);
    }

}
