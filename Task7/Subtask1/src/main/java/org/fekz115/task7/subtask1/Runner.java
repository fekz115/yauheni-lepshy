package org.fekz115.task7.subtask1;

import org.fekz115.task7.subtask1.domain.Profile;
import org.fekz115.task7.subtask1.domain.ProfileType;
import org.fekz115.task7.subtask1.implementations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Runner {

    public static void main(String[] args) {

        Presenter presenter = new LoggerPresenter();

        LoginGenerator loginGenerator = new LoginGenerator();
        PasswordGenerator passwordGenerator = new PasswordGenerator();

        List<Profile> profiles = new ArrayList<>();

        Registrator registrator = new Registrator(profiles);
        Transformer transformer = new Transformer();

        String[] names = {"Emma" ,"olivia" ,"Ava" ,"isabella" ,"sophia" ,"Charlotte" ,"mia" ,"Amelia" ,"Harper" ,"evelyn" ,"Abigail" ,"emily" ,"Elizabeth" ,"Mila" ,"Ella" ,"Avery" ,"Sofia" ,"Camila" ,"Aria" ,"Scarlett" ,"Victoria" ,"Madison" ,"Luna" ,"Grace" ,"Chloe" ,"Penelope" ,"Layla" ,"Riley" ,"Zoey" ,"Nora"};
        
        for(int i = 0; i < 30; i++) {
            Profile profile = new Profile(
                    i,
                    names[i],
                    loginGenerator.get(),
                    passwordGenerator.get(),
                    ProfileType.USER
            );
            transformer.accept(profile);
            registrator.apply(profile);
        }

        List<Profile> simpleSortedProfiles = profiles.stream().sorted(new SimpleProfileComparator()).collect(Collectors.toList());
        List<Profile> complexSortedProfiles = profiles.stream().sorted(new ComplexProfileComparator()).collect(Collectors.toList());

        presenter.logAllProfiles(profiles);
        presenter.logAllProfiles(simpleSortedProfiles);
        presenter.logAllProfiles(complexSortedProfiles);

    }

}
