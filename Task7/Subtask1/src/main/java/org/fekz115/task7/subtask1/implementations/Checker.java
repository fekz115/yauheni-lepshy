package org.fekz115.task7.subtask1.implementations;

import org.fekz115.task7.subtask1.domain.Profile;

import java.util.function.Predicate;

public class Checker implements Predicate<Profile> {

    @Override
    public boolean test(Profile profile) {
        return !profile.getPassword().isEmpty() && !profile.getLogin().isEmpty();
    }

}
