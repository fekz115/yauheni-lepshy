package org.fekz115.task7.subtask1.implementations;

import org.fekz115.task7.subtask1.domain.Profile;

import java.util.Comparator;

public class ComplexProfileComparator implements Comparator<Profile> {

    public int compare(Profile profile, Profile t1) {
        if(profile.getType() == t1.getType()) {
            return Integer.compare(profile.getId(), t1.getId());
        } else {
            return profile.getType().compareTo(t1.getType());
        }
    }

}
