package org.fekz115.task7.subtask1.implementations;

import org.fekz115.task7.subtask1.Presenter;
import org.fekz115.task7.subtask1.domain.Profile;

public class ConsolePresenter implements Presenter {

    @Override
    public void logProfile(Profile profile) {
        System.out.println(profile);
    }

}
