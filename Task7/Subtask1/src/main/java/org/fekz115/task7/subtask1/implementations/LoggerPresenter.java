package org.fekz115.task7.subtask1.implementations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fekz115.task7.subtask1.Presenter;
import org.fekz115.task7.subtask1.domain.Profile;

public class LoggerPresenter implements Presenter {

    private static final Logger logger = LogManager.getLogger(LoggerPresenter.class);

    @Override
    public void logProfile(Profile profile) {
        logger.info(profile);
    }
}
