package org.fekz115.task7.subtask1.implementations;

import java.util.Random;
import java.util.function.Supplier;

public class LoginGenerator implements Supplier<String> {

    private Random random = new Random();

    @Override
    public String get() {
        StringBuilder ans = new StringBuilder();
        int length = 4 + random.nextInt(5);
        for(int i = 0; i < length; i++){
            ans.append((char) (random.nextInt('z' - 'a')+'a'));
        }
        ans.append(random.nextInt(1000));
        return ans.toString();
    }

}
