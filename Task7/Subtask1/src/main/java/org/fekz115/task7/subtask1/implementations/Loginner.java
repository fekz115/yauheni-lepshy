package org.fekz115.task7.subtask1.implementations;

import org.fekz115.task7.subtask1.domain.Profile;

import java.util.List;
import java.util.function.BiFunction;

public class Loginner implements BiFunction<String, String, Profile> {

    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }

    private List<Profile> profiles;

    @Override
    public Profile apply(String s, String s2) {
        if(profiles != null) {
           return profiles.stream().filter(x -> x.getLogin().equals(s) && x.getPassword().equals(s2)).findFirst().get();
        }
        return null;
    }
}
