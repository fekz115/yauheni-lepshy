package org.fekz115.task7.subtask1.implementations;

import java.util.Random;
import java.util.function.Supplier;

public class PasswordGenerator implements Supplier<String> {

    private Random random = new Random();

    @Override
    public String get() {
        StringBuilder ans = new StringBuilder();
        int length = 4 + random.nextInt(4);
        for(int i = 0; i < length; i++) {
            if(random.nextInt() % 2 ==0) {
                ans.append((char)(random.nextInt('z'-'a')+'a'));
            } else {
                ans.append(random.nextInt(10));
            }
        }
        return ans.toString();
    }
}
