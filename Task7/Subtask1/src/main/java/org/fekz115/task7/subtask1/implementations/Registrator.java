package org.fekz115.task7.subtask1.implementations;

import org.fekz115.task7.subtask1.domain.Profile;

import java.util.List;
import java.util.function.Function;

public class Registrator implements Function<Profile, Boolean> {

    public Registrator(List<Profile> profiles) {
        this.profiles = profiles;
        checker = new Checker();
    }

    private List<Profile> profiles;
    private Checker checker;

    @Override
    public Boolean apply(Profile profile) {
        if(checker.test(profile)) {
            profiles.add(profile);
            return true;
        }
        return false;
    }
}
