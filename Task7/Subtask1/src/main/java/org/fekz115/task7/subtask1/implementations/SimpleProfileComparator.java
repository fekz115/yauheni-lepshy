package org.fekz115.task7.subtask1.implementations;

import org.fekz115.task7.subtask1.domain.Profile;

import java.util.Comparator;

public class SimpleProfileComparator implements Comparator<Profile> {

    public int compare(Profile profile, Profile t1) {
        return profile.getName().compareTo(t1.getName());
    }

}
