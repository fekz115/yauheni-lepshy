package org.fekz115.task7.subtask1.implementations;

import org.fekz115.task7.subtask1.domain.Profile;

import java.util.function.Consumer;

public class Transformer implements Consumer<Profile> {

    @Override
    public void accept(Profile profile) {
        if(!profile.getName().isEmpty() && Character.isLowerCase(profile.getName().charAt(0))) {
            String name = profile.getName();
            char c = Character.toUpperCase(name.charAt(0));
            name = c + name.substring(1);
            profile.setName(name);
        }
    }

}
