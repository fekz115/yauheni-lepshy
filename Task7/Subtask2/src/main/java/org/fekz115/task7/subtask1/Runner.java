package org.fekz115.task7.subtask1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fekz115.task7.subtask1.domain.Author;
import org.fekz115.task7.subtask1.domain.Book;
import org.fekz115.task7.subtask1.util.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class Runner {

    private static Logger logger = LogManager.getLogger(Runner.class);

    public static void main(String[] args) {
        List<Book> books = Utils.getBooks();
        List<Author> authors = Utils.getAuthors();
        print("1) проверить есть ли книги с количеством страниц более 200 - " + books.stream().anyMatch(x -> x.getLength() > 200));
        print("2) проверить все ли книги имеют более 200 страниц - " + books.stream().allMatch(x -> x.getLength() > 200));
        int maxLength = books.stream().mapToInt(Book::getLength).max().getAsInt();
        print("3) найти книги с наибольшим числом страниц - " + books.stream().filter(book -> book.getLength() == maxLength).collect(Collectors.toList()));
        int minLength = books.stream().mapToInt(Book::getLength).min().getAsInt();
        print("3) найти книги с наибольшим числом страниц - " + books.stream().filter(book -> book.getLength() == minLength).collect(Collectors.toList()));
        print("4) найти книги с одним автором - " + books.stream().filter(book -> book.getAuthors().size() == 1).collect(Collectors.toList()));
        print("5) отсортировать книги по количеству страниц, а затем по названию - " + books.stream().sorted((Book book1, Book book2) -> {
            if(book1.getLength() != book2.getLength())
                return Integer.compare(book1.getLength(), book2.getLength());
            else
                return book1.getName().compareTo(book2.getName());
        }).collect(Collectors.toList()));
        print("6) получить список всех уникальных названий книг - " + books.stream().map(Book::getName).distinct().collect(Collectors.toList()));
        print("7) получить уникальный список авторов книг с количеством страниц менее 200 - " + books.stream().filter(x -> x.getLength() > 200).flatMap(x -> x.getAuthors().stream()).distinct().collect(Collectors.toList()));
    }

    private static void print(Object s) {
        logger.info(s);
    }

}
