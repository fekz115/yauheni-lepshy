package org.fekz115.task7.subtask1.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Book {

    private String name;
    private int length;
    private List<Author> authors = new ArrayList<Author>();

    public Book(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void addAuthor(Author author) {
        authors.add(author);
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", length=" + length +
                ", authors=" + authors.stream().map(Author::getName).collect(Collectors.toList()) +
                '}';
    }
}
