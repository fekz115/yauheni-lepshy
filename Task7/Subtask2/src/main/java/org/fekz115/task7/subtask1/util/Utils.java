package org.fekz115.task7.subtask1.util;

import org.fekz115.task7.subtask1.domain.Author;
import org.fekz115.task7.subtask1.domain.Book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {

    private static final List<String> authorNames = Arrays.asList("Aasdf", "Qqwerqert", "Bsfgsdfg", "Zcvb", "Ndsfgsdg");
    private static final List<String> bookNames = Arrays.asList("1AASD", "2DFDSF", "4fgsdfg", "7sfg", "8asdfasdf", "9adxvzx");

    private static List<Author> authors;
    private static List<Book> books;

    private static int getAge() {
        return 20 + (int)(Math.random() * 40);
    }

    private static int getLength() {
        return 50 + (int)(Math.random() * 800);
    }

    private static final int links = authorNames.size() * bookNames.size();

    static {
        authors = new ArrayList<Author>(authorNames.size());
        books = new ArrayList<Book>(bookNames.size());
        for(String name : authorNames) {
            authors.add(new Author(name, getAge()));
        }
        for(String name : bookNames) {
            books.add(new Book(name, getLength()));
        }
        for(int i = 0; i < links; i++) {
            int bookIndex = (int)(Math.random() * books.size());
            int authorIndex = (int)(Math.random() * authors.size());
            if(Math.random() > .5) {
                books.get(bookIndex).addAuthor(authors.get(authorIndex));
                authors.get(authorIndex).addBook(books.get(bookIndex));
            }
        }
    }

    public static List<Author> getAuthors() {
        return authors;
    }

    public static List<Book> getBooks() {
        return books;
    }
}
