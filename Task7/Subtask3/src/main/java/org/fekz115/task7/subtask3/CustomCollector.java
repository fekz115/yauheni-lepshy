package org.fekz115.task7.subtask3;

import org.fekz115.task7.subtask3.domain.Entry;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CustomCollector implements Collector<Entry, ConcurrentMap<Month, Entry>, List<LocalDate>> {

    @Override
    public Supplier<ConcurrentMap<Month, Entry>> supplier() {
        return ConcurrentHashMap::new;
    }

    @Override
    public BiConsumer<ConcurrentMap<Month, Entry>, Entry> accumulator() {
        return (map, entry) -> {
            if(!map.containsKey(entry.getDate().getMonth())) {
                map.put(entry.getDate().getMonth(), entry);
            } else if (map.get(entry.getDate().getMonth()).getAbstractInt() < entry.getAbstractInt()) {
                map.put(entry.getDate().getMonth(), entry);
            }
        };
    }

    @Override
    public BinaryOperator<ConcurrentMap<Month, Entry>> combiner() {
        return (x, y) -> {
            y.forEach((key, value) -> accumulator().accept(x, value));
            return x;
        };
    }

    @Override
    public Function<ConcurrentMap<Month, Entry>, List<LocalDate>> finisher() {
           return map -> map.values().stream().map(Entry::getDate).collect(Collectors.toList());
    }

    @Override
    public Set<Characteristics> characteristics() {
        Set<Characteristics> ans = new HashSet<>();
        ans.add(Characteristics.CONCURRENT);
        return ans;
    }
}
