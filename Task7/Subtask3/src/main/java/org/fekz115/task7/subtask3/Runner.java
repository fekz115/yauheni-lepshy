package org.fekz115.task7.subtask3;

import org.fekz115.task7.subtask3.domain.Entry;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Runner {


    private static Comparator<Entry> comparator = Comparator.comparingInt(Entry::getAbstractInt);

    public static void main(String[] args) {
        List<Entry> entries = generateEntriesList();

        Entry min = entries.stream().min(comparator).get();
        System.out.println(min);
        Entry max = entries.stream().max(comparator).get();
        System.out.println(max);
        double avg = entries.stream().mapToInt(Entry::getAbstractInt).average().getAsDouble();
        System.out.println(avg);
        entries
                .stream()
                .collect(Collectors.groupingBy(x -> x.getDate().getMonth(), Collectors.toList()))
                .forEach((key, value) -> {
                    System.out.println("Month: " + key.name());
                    System.out.println(value.stream().max(comparator).get());
                    System.out.println(value.stream().min(comparator).get());
                    System.out.println(value.stream().mapToInt(Entry::getAbstractInt).average());
                });
        List<LocalDate> answer = entries.parallelStream().collect(new CustomCollector());
    }

    private static List<Entry> generateEntriesList() {
        List<Entry> entries = new ArrayList<>();
        LocalDate endDate = LocalDate.now().plusYears(1);
        for(LocalDate date = LocalDate.now(); date.isBefore(endDate); date = date.plusDays(1)) {
            entries.add(new Entry(date, (int)(Math.random() * 500)));
        }
        return entries;
    }



}
