package org.fekz115.task7.subtask3.domain;

import java.time.LocalDate;
import java.util.Comparator;

public class Entry {

    private LocalDate date;
    private int abstractInt;

    public Entry(LocalDate date, int abstractInt) {
        this.date = date;
        this.abstractInt = abstractInt;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getAbstractInt() {
        return abstractInt;
    }

    public void setAbstractInt(int abstractInt) {
        this.abstractInt = abstractInt;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "date=" + date +
                ", abstractInt=" + abstractInt +
                '}';
    }
}
